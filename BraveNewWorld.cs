﻿using System;

namespace BraveNewWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            int worldMoney = 0;
            int heroMoney = 0;
            int heroLog = 0;
            int worldLog = 0;
            char[,] mapArray = new char[15, 30];
            int heroX = mapArray.GetLength(1) / 2;
            int heroY = mapArray.GetLength(0) / 2 + 2;
            int deltaX = 0;
            int deltaY = 0;

            Console.CursorVisible = false;
            MapCreation(mapArray, ref worldMoney, ref worldLog);
            GameInfo(mapArray, heroX, heroY, deltaY, deltaX, heroMoney, heroLog, worldMoney);
            DrawMap(mapArray);

            while (worldMoney > 0)
            {
                MoveHero(mapArray, ref heroX, ref heroY, ref deltaX, ref deltaY, ref heroLog);
                DrawHero(heroX, heroY);
                GameInfo(mapArray, heroX, heroY, deltaY, deltaX, heroMoney, heroLog, worldMoney);
                ContolHero(mapArray, heroX, heroY, ref deltaX, ref deltaY);
                RecalculatingNumberOfResourcesOnMap(mapArray, ref heroMoney, ref heroLog, ref worldMoney, ref worldLog);
                DrawMap(mapArray);
            }

            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Деньги собраны.Нажмите любую кнопку для выхода");
            Console.ReadKey();
        }

        static int CountingNumberOfCharactersOnMap(Char charToCheck, char[,] mapArray)
        {
            int sumChar = 0;

            for (int i = 0; i < mapArray.GetLength(0); i++)
                for (int j = 0; j < mapArray.GetLength(1); j++)
                    if (mapArray[i, j] == charToCheck)
                        sumChar++;

            return sumChar;
        }

        static void RecalculatingNumberOfResourcesOnMap(char[,] mapArray, ref int heroMoney, ref int heroLog, ref int worldMoney, ref int worldLog)
        {
            char wood = '&';
            char money = '$';

            heroMoney += worldMoney - CountingNumberOfCharactersOnMap(money, mapArray);
            worldMoney = CountingNumberOfCharactersOnMap(money, mapArray);
            heroLog += worldLog - CountingNumberOfCharactersOnMap(wood, mapArray);
            worldLog = CountingNumberOfCharactersOnMap(wood, mapArray);
        }

        static void MapCreation(char[,] mapArray, ref int creationMoney, ref int creationWood)
        {
            Random random = new Random();
            char edgeOfWorld = '#';
            char water = '`';
            char wood = '&';
            char money = '$';
            char field = ' ';

            for (int i = 0; i < mapArray.GetLength(0); i++)
            {
                for (int j = 0; j < mapArray.GetLength(1); j++)
                {
                    if ((i == 0) || (j == 0) || (i == (mapArray.GetLength(0) - 1)) || (j == (mapArray.GetLength(1) - 1)))
                    {
                        mapArray[i, j] = edgeOfWorld;
                    }
                    else if (i > (mapArray.GetLength(0) / 5 - 1) && i < (mapArray.GetLength(0) / 5 + 4))
                    {
                        mapArray[i, j] = water;
                    }
                    else
                    {
                        switch (random.Next(0, 10))
                        {
                            case 0:
                            case 1:
                            case 2:
                                if (i > (mapArray.GetLength(0) / 5 - 2) && i < (mapArray.GetLength(0) / 5 + 5))
                                    mapArray[i, j] = water;
                                else
                                    mapArray[i, j] = field;
                                break;
                            case 3:
                            case 4:
                                mapArray[i, j] = wood;
                                creationWood++;
                                break;
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                mapArray[i, j] = field;
                                break;
                            case 9:
                                mapArray[i, j] = money;
                                creationMoney++;
                                break;
                        }
                    }
                }
            }
        }

        static void DrawMap(char[,] mapArray)
        {
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < mapArray.GetLength(0); i++)
            {
                for (int j = 0; j < mapArray.GetLength(1); j++)
                {
                    Console.Write(mapArray[i, j]);
                }
                Console.WriteLine();
            }
        }

        static void MoveHero(char[,] mapArray, ref int heroX, ref int heroY, ref int deltaX, ref int deltaY, ref int heroLog)
        {
            char edgeOfWorld = '#';
            char water = '`';

            if ((mapArray[heroY + deltaY, heroX + deltaX] != edgeOfWorld) && (mapArray[heroY + deltaY, heroX + deltaX] != water || heroLog > 0))
            {
                heroY += deltaY;
                heroX += deltaX;
                deltaX = 0;
                deltaY = 0;
            }
        }

        static void DrawHero(int heroX, int heroY)
        {
            char heroPosition = 'H';

            Console.SetCursorPosition(heroX, heroY);
            Console.Write(heroPosition);
        }

        static void ControlHero(char[,] mapArray, int heroX, int heroY, ref int deltaX, ref int deltaY)
        {
            deltaX = 0;
            deltaY = 0;
            ConsoleKeyInfo charKey;
            charKey = Console.ReadKey();

            switch (charKey.Key)
            {
                case ConsoleKey.UpArrow:
                case ConsoleKey.DownArrow:
                case ConsoleKey.LeftArrow:
                case ConsoleKey.RightArrow:
                    ChangeDirection(charKey, ref deltaX, ref deltaY);
                    break;
                case ConsoleKey.Spacebar:
                    ItemsCollecting(mapArray, heroX, heroY);
                    break;
            }
        }

        static void ChangeDirection(ConsoleKeyInfo charKey, ref int deltaX, ref int deltaY)
        {
            deltaX = 0;
            deltaY = 0;

            switch (charKey.Key)
            {
                case ConsoleKey.UpArrow:
                    deltaY--;
                    break;
                case ConsoleKey.DownArrow:
                    deltaY++;
                    break;
                case ConsoleKey.LeftArrow:
                    deltaX--;
                    break;
                case ConsoleKey.RightArrow:
                    deltaX++;
                    break;
            }
        }


        static void ItemsCollecting(char[,] mapArray, int heroX, int heroY)
        {
            char money = '$';
            char wood = '&';
            char field = ' ';

            if (mapArray[heroY, heroX] == money || mapArray[heroY, heroX] == wood)
                mapArray[heroY, heroX] = field;
        }

        static void GameInfo(char[,] mapArray, int heroX, int heroY, int deltaY, int deltaX, int heroMoney, int heroLog, int worldMoney)
        {
            char edgeOfWorld = '#';
            char water = '`';
            char wood = '&';
            char money = '$';
            string infoCode = "";
            int infoCursorPosition = mapArray.GetLength(0) + 2;

            if ((mapArray[heroY, heroX] == money) || (mapArray[heroY, heroX] == wood))
                infoCode += mapArray[heroY, heroX];
            if ((mapArray[heroY + deltaY, heroX + deltaX] == edgeOfWorld) || (mapArray[heroY + deltaY, heroX + deltaX] == water && heroLog == 0))
                infoCode += mapArray[heroY + deltaY, heroX + deltaX];
            while (infoCode.Length < 3)
                infoCode += ' ';

            Console.SetCursorPosition(0, infoCursorPosition);
            Console.WriteLine("Собери все деньги на карте\nДенег на карте осталось: " + worldMoney + "      ");
            Console.SetCursorPosition(0, infoCursorPosition + 3);

            for (int i = 0; i < 3; i++)
            {
                switch (infoCode[i])
                {
                    case '$':
                        Console.WriteLine("Нажмите Пробел чтобы поднять деньги                               ");
                        break;
                    case '&':
                        Console.WriteLine("Нажмите Пробел чтобы срубить дерево                               ");
                        break;
                    case '#':
                        Console.WriteLine("Дальше край мира, я не пойду туда                                 ");
                        break;
                    case '`':
                        Console.WriteLine("Дальше вода, я не умею плавать,чтобы пойти туда мне нужно бревно  ");
                        break;
                    case ' ':
                        Console.WriteLine("                                                                  ");
                        break;
                    default:
                        break;
                }
            }

            Console.SetCursorPosition(0, infoCursorPosition + 6);
            Console.WriteLine("Сейчас в сумке:");
            Console.WriteLine("Денег: " + heroMoney);
            Console.WriteLine("Бревен: " + heroLog);
        }
    }
}
