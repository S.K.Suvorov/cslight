﻿using System;

namespace PersonnelRecords
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] personalArray = { { "Иванов", "Семен", "Олегович" }, { "Иванов", "Дмитрий", "Андреевич" }, { "Петров", "Антон", "Владимирович" }, { "Смирнов", "Иван", "Иванович" } };
            string[] positionArray = { "Инженер", "Мастер", "Конструктор", "Cлесарь" };
            string mainCommand = "";

            while (mainCommand.ToLower() != "выход")
            {
                Console.Clear();
                Console.WriteLine($"Добро пожаловать! Выберите команду из списка:\n\n" +
                    $"Вывести Досье - для вывода списка сотрудников\n" +
                    $"Добавить Досье - добавить новое досье\n" +
                    $"Удалить Досье  - Удалить досье из списка\n" +
                    $"Поиск  - поиск досье\n" +
                    $"Выход - выход");

                mainCommand = Console.ReadLine().ToLower();
                switch (mainCommand)
                {
                    case "вывести досье":
                        OutPutInfo(ref personalArray, ref positionArray, ref mainCommand);
                        break;
                    case "добавить досье":
                        AddInfo(ref personalArray, ref positionArray);
                        break;
                    case "удалить досье":
                        DeleteInfo(ref personalArray, ref positionArray);
                        break;
                    case "поиск":
                        Search(ref personalArray, ref positionArray, ref mainCommand);
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда.Проверте правильность ввода.Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }

        static void Search(ref string[,] personalArray, ref string[] positionArray, ref string mainCommand)
        {
            string surname = "";
            string info = "";
            string subCommand = "";

            Console.Clear();
            Console.WriteLine("Введите фамилию для поиска:");

            surname = Console.ReadLine();

            for (int i = 0; i < personalArray.GetLength(0); i++)
            {
                if (surname.ToLower() == personalArray[i, 0].ToLower())
                {
                    info += (i+1) + ". ";
                    for (int j = 0; j < personalArray.GetLength(1); j++)
                    {
                        info += personalArray[i, j] + " ";
                    }
                    info += " - " + positionArray[i] + "\n";
                }
            }

            if (info != "")
                info = "Найдены следующие совпадения:\n" + info;
            else
                info = "Совпадений нет";

            while ((subCommand.ToLower() != "назад") && (subCommand != "выход"))
            {
                Console.WriteLine(info + "\n\n Введите команду: " +
                "\n Назад - для возврата в основное меню" +
                "\n Удалить Досье - для удаления досье" +
                "\n Выход - для выхода \n");
                subCommand = Console.ReadLine().ToLower();
                switch (subCommand)
                {
                    case "выход":
                        mainCommand = "выход";
                        break;
                    case "назад":
                        break;
                    case "удалить досье":
                        DeleteInfo(ref personalArray, ref positionArray);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }

        static void DeleteInfo(ref string[,] personalArray, ref string[] positionArray)
        {
            int num = -1;
            int shift = 0;
            string[,] tempPersonalArray = new string[personalArray.GetLength(0) - 1, personalArray.GetLength(1)];
            string[] tempPositionArray = new string[positionArray.Length - 1];

            Console.WriteLine($"Введите номер записи для удаления от 1 до {personalArray.GetLength(0)}:");
            while ((num > (personalArray.GetLength(0) - 1)) || (num < 0))
            {
                num = NumderInput() - 1;
                if ((num > (personalArray.GetLength(0) - 1)) || (num < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер записи для удаления от 1 до {personalArray.GetLength(0)}:");
            }


            for (int i = 0; i < tempPersonalArray.GetLength(0); i++)
            {
                for (int j = 0; j < tempPersonalArray.GetLength(1); j++)
                {
                    if (i == num)
                        shift = 1;
                    tempPersonalArray[i, j] = personalArray[i + shift, j];
                    tempPositionArray[i] = positionArray[i + shift];
                }
            }

            ChangeConfirmation(ref personalArray, ref positionArray, ref tempPersonalArray, ref tempPositionArray);
        }

        static void OutPutInfo(ref string[,] personalArray, ref string[] positionArray, ref string mainCommand)
        {
            string info;
            string subCommand = "";

            while ((subCommand.ToLower() != "назад") && (subCommand != "выход"))
            {
                info = "";
                Console.Clear();
                Console.WriteLine("Список сотрудников:\n\n");
                for (int i = 0; i < personalArray.GetLength(0); i++)
                {
                    info += (i+1) + ". ";
                    for (int j = 0; j < personalArray.GetLength(1); j++)
                        info += personalArray[i, j] + " ";
                    info += " - " + positionArray[i] + "\n";
                }
                Console.Clear();
                Console.WriteLine(info + "" +
                    "\n\n Введите команду: " +
                    "\nУдалить Досье - для удаления досье" +
                    "\nДобавить Досье - добавить новое досье" +
                    "\nНазад - для возврата в основное меню" +
                    "\nВыход - для выхода \n");
                subCommand = Console.ReadLine().ToLower();
                switch (subCommand)
                {
                    case "выход":
                        mainCommand = "выход";
                        break;
                    case "назад":
                        break;
                    case "удалить досье":
                        DeleteInfo(ref personalArray, ref positionArray);
                        break;
                    case "добавить досье":
                        AddInfo(ref personalArray, ref positionArray);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }

        static void AddInfo(ref string[,] personalArray, ref string[] positionArray)
        {

            string[,] tempPersonalArray = new string[personalArray.GetLength(0) + 1, personalArray.GetLength(1)];
            string[] tempPositionArray = new string[positionArray.Length + 1];

            for (int i = 0; i < personalArray.GetLength(0); i++)
            {
                for (int j = 0; j < personalArray.GetLength(1); j++)
                {
                    tempPersonalArray[i, j] = personalArray[i, j];
                    tempPositionArray[i] = positionArray[i];
                }
            }

            Console.Clear();
            Console.WriteLine("Введите Фамилию нового сотрудника");
            tempPersonalArray[personalArray.GetLength(0), 0] = Console.ReadLine();
            Console.WriteLine("Введите Имя нового сотрудника");
            tempPersonalArray[personalArray.GetLength(0), 1] = Console.ReadLine();
            Console.WriteLine("Введите Отчество нового сотрудника");
            tempPersonalArray[personalArray.GetLength(0), 2] = Console.ReadLine();
            Console.WriteLine("Введите Должность нового сотрудника");
            tempPositionArray[positionArray.Length] = Console.ReadLine();

            ChangeConfirmation(ref personalArray, ref positionArray, ref tempPersonalArray, ref tempPositionArray);
        }

        static int NumderInput()
        {
            int num = 0;
            bool isNum = false;

            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }

        static void ChangeConfirmation(ref string[,] personalArray, ref string[] positionArray, ref string[,] tempPersonalArray, ref string[] tempPositionArray)
        {
            string youSure = "";
            Console.WriteLine("Вы уверены? Да/Нет");
            youSure = Console.ReadLine();
            switch (youSure.ToLower())
            {
                case "да":
                    Console.WriteLine("Данные изменены. Нажмите Enter для возвращения в основное меню");
                    personalArray = tempPersonalArray;
                    positionArray = tempPositionArray;
                    Console.ReadKey();
                    break;
                case "нет":
                    Console.WriteLine("Отмена изменений. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
