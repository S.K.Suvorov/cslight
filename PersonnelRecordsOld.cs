﻿using System;

namespace PersonnelRecords
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] personalArray = { { "Иванов", "Семен", "Олегович" }, { "Иванов", "Дмитрий", "Андреевич" }, { "Петров", "Антон", "Владимирович" }, { "Смирнов", "Иван", "Иванович" } };
            string[] positionArray = { "Инженер", "Мастер", "Конструктор", "Cлесарь" };
            string mainCommand = "";

            while (mainCommand.ToLower() != "выход")
            {
                Console.Clear();
                Console.WriteLine($"Добро пожаловать! Выберите команду из списка:\n\n" +
                    $"Вывести Досье - для доступа к истории изменений\n" +
                    $"Добавить Досье - добавить новое досье\n" +
                    $"Удалить Досье  - изменение имени пользователя\n" +
                    $"Поиск  - поиск досье\n" +
                    $"Выход - выход");

                mainCommand = Console.ReadLine().ToLower();
                switch (mainCommand)
                {
                    case "вывести досье":
                        OutPutInfo(personalArray, positionArray, ref mainCommand);
                        break;
                    case "добавить досье":
                        AddInfo(ref personalArray, ref positionArray);
                        break;
                    case "удалить досье":
                        DeleteInfo(ref personalArray, ref positionArray);
                        break;
                    case "поиск":
                        Search(personalArray, positionArray, ref mainCommand);
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда.Проверте правильность ввода.Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }
        static void Search(string[,] array1, string[] array2, ref string mainCommand)
        {
            string surname = "";
            string info = "";
            string subCommand = "";

            Console.Clear();
            Console.WriteLine("Введите фамилию для поиска:");

            surname = Console.ReadLine();

            for (int i = 0; i < array1.GetLength(0); i++)
            {
                if (surname.ToLower() == array1[i, 0].ToLower())
                {
                    info += i + ". ";
                    for (int j = 0; j < array1.GetLength(1); j++)
                    {
                        info += array1[i, j] + " ";
                    }
                    info += " - " + array2[i] + "\n";
                }
            }

            if (info != "")
                info = "Найдены следующие совпадения:\n" + info;
            else
                info = "Совпадений нет";

            while ((subCommand.ToLower() != "назад") && (subCommand != "выход"))
            {
                Console.WriteLine(info + "\n\n Введите команду: " +
                "\n Назад - для возврата в основное меню" +
                "\n Удалить Досье - для удаления досье" +
                "\n Выход - для выхода \n");
                subCommand = Console.ReadLine().ToLower();
                switch (subCommand)
                {
                    case "выход":
                        mainCommand = "выход";
                        break;
                    case "назад":
                        break;
                    case "удалить досье":
                        DeleteInfo(ref array1, ref array2);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }
        static void DeleteInfo(ref string[,] array1, ref string[] array2)
        {
            int num = -1;
            int shift = 0;
            string youSure = "";
            string[,] tempArray1 = new string[array1.GetLength(0) - 1, array1.GetLength(1)];
            string[] tempArray2 = new string[array2.Length - 1];

            Console.WriteLine($"Введите номер записи для удаленияот 0 до {(array1.GetLength(0) - 1)}:");
            while ((num > (array1.GetLength(0)-1)) || (num < 0))
            {
                num = NumderInput();
                if ((num > (array1.GetLength(0)-1))|| (num < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер записи для удаления от 0 до {(array1.GetLength(0) - 1)}:");
            }


            for (int i = 0; i < tempArray1.GetLength(0); i++)
            {
                for (int j = 0; j < tempArray1.GetLength(1); j++)
                {
                    if (i == num)
                        shift = 1;
                    tempArray1[i, j] = array1[i + shift, j];
                    tempArray2[i] = array2[i + shift];
                }
            }

            Console.WriteLine("Вы уверены? Да/Нет");
            youSure = Console.ReadLine();
            switch (youSure.ToLower())
            {
                case "да":
                    Console.WriteLine("Запись удалена. Нажмите Enter для возвращения в основное меню");
                    array1 = tempArray1;
                    array2 = tempArray2;
                    Console.ReadKey();
                    break;
                case "нет":
                    Console.WriteLine("Удаление отменено. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
            }
        }
        static void OutPutInfo(string[,] array1, string[] array2, ref string mainCommand)
        {
            string info;
            string subCommand = "";
            
            while ((subCommand.ToLower() != "назад") && (subCommand != "выход"))
            {
                info = "";
                Console.Clear();
                Console.WriteLine("Список сотрудников:\n\n");
                for (int i = 0; i < array1.GetLength(0); i++)
                {
                    info += i + ". ";
                    for (int j = 0; j < array1.GetLength(1); j++)
                        info += array1[i, j] + " ";
                    info += " - " + array2[i] + "\n";
                }
                Console.Clear();
                Console.WriteLine(info + "" +
                    "\n\n Введите команду: " +
                    "\nУдалить Досье - для удаления досье" +
                    "\nДобавить Досье - добавить новое досье" +
                    "\nНазад - для возврата в основное меню" +
                    "\nВыход - для выхода \n");
                subCommand = Console.ReadLine().ToLower();
                switch (subCommand)
                {
                    case "выход":
                        mainCommand = "выход";
                        break;
                    case "назад":
                        break;
                    case "удалить досье":
                        DeleteInfo(ref array1, ref array2);
                        break;
                    case "добавить досье":
                        AddInfo(ref array1, ref array2);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }

        }
        static void AddInfo(ref string[,] array1, ref string[] array2)
        {
            string youSure = "";
            string[,] tempArray1 = new string[array1.GetLength(0) + 1, array1.GetLength(1)];
            string[] tempArray2 = new string[array2.Length + 1];

            for (int i = 0; i < array1.GetLength(0); i++)
            {
                for (int j = 0; j < array1.GetLength(1); j++)
                {
                    tempArray1[i, j] = array1[i, j];
                    tempArray2[i] = array2[i];
                }
            }

            Console.Clear();
            Console.WriteLine("Введите Фамилию нового сотрудника");
            tempArray1[array1.GetLength(0), 0] = Console.ReadLine();
            Console.WriteLine("Введите Имя нового сотрудника");
            tempArray1[array1.GetLength(0), 1] = Console.ReadLine();
            Console.WriteLine("Введите Отчество нового сотрудника");
            tempArray1[array1.GetLength(0), 2] = Console.ReadLine();
            Console.WriteLine("Введите Должность нового сотрудника");
            tempArray2[array2.Length] = Console.ReadLine();

            Console.WriteLine("Вы уверены? Да/Нет");
            youSure = Console.ReadLine();
            switch (youSure.ToLower())
            {
                case "да":
                    Console.WriteLine("Запись сохранена. Нажмите Enter для возвращения в основное меню");
                    array1 = tempArray1;
                    array2 = tempArray2;
                    Console.ReadKey();
                    break;
                case "нет":
                    Console.WriteLine("Запись отменена. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в основное меню");
                    Console.ReadKey();
                    break;
            }
        }
        static int NumderInput()
        {
            int num = 0;
            bool isNum = false;

            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
    }
}
