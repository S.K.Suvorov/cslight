﻿using System;

namespace ConsoleMenu
{
    class Program
    {
        static void Main(string[] args)
        {
            string password = "sunrise";
            int numOfAttempts = 3;
            string name = "User";
            bool acces = false;
            string history = "";
            string mainCommand = "";
            string subCommand = "";

            for (int i = 0; i < numOfAttempts; i++)
            {
                Console.WriteLine("Количество оставшихся попыток " + (numOfAttempts - i));
                Console.WriteLine("Введите пароль  ");
                if (password == Console.ReadLine())
                {
                    Console.WriteLine("Доступ подтвержден.Нажмите любую кнопку для продолжения");
                    acces = true;
                    Console.ReadKey();
                    break;
                }
            }

            if (acces==true)
            {
                while (mainCommand != "Esc")
                {
                    subCommand = "";
                    Console.Clear();
                    Console.WriteLine($"Добро пожаловать, {name}! Выберите команду из списка:\n" +
                        $"History - для доступа к истории изменений\n" +
                        $"ChangeName - изменение имени пользователя\n" +
                        $"ChangeConsoleColor - изменение цвета консоли\n" +
                        $"ChangeFontColor - изменение цвета шрифта\n" +
                        $"Esc - выход");

                    mainCommand = Console.ReadLine();
                    switch (mainCommand)
                    {
                        case "History":    
                            while ((subCommand != "Back" ) && ( subCommand != "Esc"))
                            {
                                Console.Clear();
                                Console.WriteLine(history + "" +
                                    "\n\n Введите команду: " +
                                    "\n Back - для возврата в основное меню" +
                                    "\n Esc - для выхода \n");
                                subCommand = Console.ReadLine();
                                switch (subCommand)
                                {
                                    case "Esc":
                                        mainCommand = "Esc";
                                        break;

                                    case "Back":
                                        break;

                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите любую кнопку для возвращения в выбранное меню");
                                        Console.ReadKey();
                                        break;
                                }
                            }
                            break;
                        case "ChangeName":
                            Console.Clear();
                            Console.WriteLine("Введите новое имя пользователя, введите Cancel для отмены:");
                            subCommand = Console.ReadLine();
                            if (subCommand!="Cancel")
                            {
                                history = $"Изменено имя пользователя: с {name} на {subCommand}\n" + history;
                                name = subCommand;
                                Console.WriteLine("Имя пользователя изменено, нажмите любую кнопку для продолжения");
                            }
                            break;
                        case "ChangeConsoleColor":
                            while (subCommand != "Back" && subCommand != "Esc")
                            {
                                Console.Clear();
                                Console.WriteLine("Изменение цвета консоли" +
                                    "\n\n Введите команду: " +
                                    "\n Blue - изменение цвета консоли на синий" +
                                    "\n Black - изменение цвета консоли на черный" +
                                    "\n Back - для возврата в основное меню" +
                                    "\n Esc - для выхода \n");
                                subCommand = Console.ReadLine();
                                switch (subCommand)
                                {
                                    case "Black":
                                        Console.BackgroundColor = ConsoleColor.Black;
                                        history = "Изменение цвета консоли на черный \n" + history;
                                        break;
                                    case "Blue":
                                        Console.BackgroundColor = ConsoleColor.Blue;
                                        history = "Изменение цвета консоли на синий \n" + history;
                                        break;
                                    case "Esc":
                                        mainCommand = "Esc";
                                        break;
                                    case "Back":
                                        break;
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите любую кнопку для возвращения в выбранное меню");
                                        Console.ReadKey();
                                        break;
                                }
                            }
                            break;
                        case "ChangeFontColor":
                            while (subCommand != "Back" && subCommand != "Esc")
                            {
                                Console.Clear();
                                Console.WriteLine("Изменение цвета шрифта" + "" +
                                    "\n\n Введите команду: " +
                                    "\n White - изменение цвета шрифта на белый" +
                                    "\n Red - изменение цвета шрифта на красный" +
                                    "\n Back - для возврата в основное меню" +
                                    "\n Esc - для выхода \n");
                                subCommand = Console.ReadLine();
                                switch (subCommand)
                                {
                                    case "White":
                                        Console.ForegroundColor= ConsoleColor.White;
                                        history = "Изменение цвета шрифта на белый \n" + history;
                                        break;
                                    case "Red":
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        history = "Изменение цвета шрифта на красный \n" + history;
                                        break;
                                    case "Esc":
                                        mainCommand = "Esc";
                                        break;
                                    case "Back":
                                        break;
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите любую кнопку для возвращения в выбранное меню");
                                        Console.ReadKey();
                                        break;
                                }
                            }
                            break;

                        case "Esc":
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Некорректная команда.Проверте правильность ввода.Нажмите любую кнопку для возвращения в выбранное меню");
                            Console.ReadKey();
                            break;

                    }
                }
            }
        }
    }
}
