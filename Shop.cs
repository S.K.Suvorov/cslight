﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            int coinCount;
            int crystalCount;
            int crystalPrice=13;
            bool enoughtCoin;
            Console.WriteLine("Cколько у Вас денег в кошельке?");
            coinCount=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Cколько кристаллов Вы хотите купить?");
            crystalCount = Convert.ToInt32(Console.ReadLine());
            enoughtCoin = coinCount >= (crystalCount * crystalPrice);
            crystalCount *=  Convert.ToInt32(enoughtCoin);
            coinCount -= crystalCount * crystalPrice;
            Console.WriteLine($"У Вас в инвентаре {crystalCount} кристал(-ов) и {coinCount} золота");
            Console.ReadKey();
        }
    }
}
