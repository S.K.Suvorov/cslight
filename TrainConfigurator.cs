﻿using System;

namespace TrainConfigurator
{
    class Program
    {
        static void Main(string[] args)
        {
            Vagon[] vagonDataBase = new Vagon[] { new Vagon("Плацкарт", 54), new Vagon("Купе", 36), new Vagon("СВ", 18) };
            Sending sending = new Sending();
            string command = "";

            while (command.ToLower() != "выход")
            {
                Console.Clear();
                sending.ShowInfo();
                Console.SetCursorPosition(0, 10);
                Console.WriteLine("Введите команду из списка: " +
                    "\nОтправление - для установки пункта отправления" +
                    "\nНазначение - для установки пункта назначения" +
                    "\nКонфигурация - для конфигурации состава поезда" +
                    "\nОтправить - для отправления состава" +
                    "\nВыход - выход из программы \n\n");

                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "назначение":
                        sending.SetDestinationPoint();
                        break;
                    case "отправление":
                        sending.SetDeparturePoint();
                        break;
                    case "конфигурация":
                        if (sending.DeparturePoint != "" && sending.DestinationPoint != "")
                        {
                            TrainConfiguration(sending, vagonDataBase, ref command);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Проверьте пункты отпраления и назначения. Нажмите любую кнопку для возврата в меню");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case "отправить":
                        Console.Clear();
                        if (sending.readyForDeparture())
                        {
                            sending = new Sending();
                            Console.WriteLine("Поезд отправлен. Нажмите любую кнопку для возврата в меню");
                        }
                        else
                        {
                            Console.WriteLine("Поезд не готов к отправке. Проверьте исходные данные. Нажмите любую кнопку для возврата в меню");
                        }
                        Console.ReadKey();
                        break;
                    case "выход":
                        break;
                    default:
                        Console.WriteLine("Некоректная команда. Нажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        break;
                }
            }
        }

        static void TrainConfiguration(Sending sending, Vagon[] vagonDataBase, ref string mainCommand)
        {
            string command = "";

            while (command.ToLower() != "выход" && command.ToLower() != "назад")
            {
                Console.Clear();
                Console.WriteLine($"Информация о поезде:" +
                    $"\nКоличество вагонов в поезде - {sending.Train.Vagons.Length}" +
                    $"\nКоличество посадочных мест - {sending.Train.SeatsNumber}" +
                    $"\nКоличество проданных мест на данное направление - {sending.SellsSeatsNumber}\n\n");

                Console.WriteLine("Введите команду из списка: " +
                    "\nАвто - для установки пункта назначения" +
                    "\nПлацкарт - для добавления вагона типа Плацкарт " +
                    "\nКупе - для добавления вагона типа Купе" +
                    "\nСВ - для добавления вагона типа СВ" +
                    "\nУдалить - для удаления вагонаВ"+
                    "\nИнфо - для вывода полной информации о поезде" +
                    "\nНазад - возврат в пердыдущее меню " +
                    "\nВыход - выход из программы  \n\n");

                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "авто":
                            int deltaSeats;
                            int vagonCount;

                            for (int i = 0; i < 3; i++)
                            {
                                deltaSeats = sending.SellsSeatsNumber - sending.Train.SeatsNumber;
                                vagonCount = deltaSeats / vagonDataBase[i].SeatsNumber;

                                if (i == 0)
                                    vagonCount--;
                                if ((i==2) && (vagonDataBase[i].SeatsNumber*vagonCount < deltaSeats))
                                    vagonCount++;

                                for (int j = 0; j < vagonCount; j++)
                                {
                                    sending.Train.AddVagon(vagonDataBase[i]);
                                }
                            }
                        break;
                    case "плацкарт":
                        sending.Train.AddVagon(vagonDataBase[0]);
                        break;
                    case "купе":
                        sending.Train.AddVagon(vagonDataBase[1]);
                        break;
                    case "св":
                        sending.Train.AddVagon(vagonDataBase[2]);
                        break;
                    case "инфо":
                        Console.Clear();
                        sending.Train.ShowInfo();
                        Console.WriteLine("\n\nНажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        break;
                    case "удалить":
                        sending.Train.DeleteVagon();
                        break;
                    case "назад":
                        break;
                    case "выход":
                        mainCommand = "выход";
                        break;
                    default:
                        Console.WriteLine("Некоректная команда. Нажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        break;
                }
            }
        }
    }

    class Vagon
    {
        public string VagonTypeName { get; private set; }
        public int SeatsNumber { get; private set; }

        public Vagon(string vagonTypeNane, int seatsNumber)
        {
            VagonTypeName = vagonTypeNane;
            SeatsNumber = seatsNumber;
        }
    }

    class PassengerTrain
    {
        public Vagon[] Vagons { get; private set; }
        public int SeatsNumber { get; private set; }

        public PassengerTrain(Vagon[] vagons)
        {
            Vagons = vagons;
            SeatsNumber = GetSeatsNumber();
        }

        public PassengerTrain()
        {
            Vagons = new Vagon[0];
        }

        public void ShowInfo()
        {
            Console.WriteLine($"Информация о поезде: \n" +
                $"Количество вагонов в поезде {Vagons.Length} \n" +
                $"Количество посадочных мест {SeatsNumber} \n" +
                $"Информация о вагонах: \n");

            for (int i = 0; i < Vagons.Length; i++)
                Console.WriteLine($"Вагон №{i + 1} тип вагона {Vagons[i].VagonTypeName} количество посадочных мест - {Vagons[i].SeatsNumber} ");
        }

        public void AddVagon(Vagon newVagon)
        {
            Vagon[] tempTrain = new Vagon[Vagons.Length + 1];

            for (int i = 0; i < Vagons.Length; i++)
                tempTrain[i] = Vagons[i];

            tempTrain[tempTrain.Length - 1] = newVagon;
            Vagons = tempTrain;
            SeatsNumber = GetSeatsNumber();
        }

        public void DeleteVagon()
        {
            if (Vagons.Length != 0)
            {
                int num = -1;
                int shift = 0;
                Vagon[] tempTrain = new Vagon[Vagons.Length - 1];

                Console.WriteLine($"Введите номер записи для удаления от 1 до {Vagons.Length}:");
                while ((num > (Vagons.Length - 1)) || (num < 0))
                {
                    num = NumderInput() - 1;
                    if ((num > (Vagons.Length - 1)) || (num < 0))
                        Console.WriteLine($"Такой номер отсутствует.\n Введите номер записи для удаления от 1 до {Vagons.Length}:");
                }

                for (int i = 0; i < tempTrain.Length; i++)
                {
                    if (i == num)
                        shift = 1;
                    tempTrain[i] = Vagons[i + shift];
                }

                Vagons = tempTrain;
                SeatsNumber = GetSeatsNumber();
            }
            else
            {
                Console.WriteLine("В поезде нет вагонов, удалять нечего");
            }
        }

        private int NumderInput()
        {
            int num = 0;
            bool isNum = false;
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }

        private int GetSeatsNumber()
        {
            SeatsNumber = 0;

            for (int i = 0; i < Vagons.Length; i++)
                SeatsNumber += Vagons[i].SeatsNumber;
            return SeatsNumber;
        }

    }

    class Sending
    {
        public string DeparturePoint { get; private set; }
        public string DestinationPoint { get; private set; }
        public int SellsSeatsNumber { get; private set; }
        public PassengerTrain Train { get; private set; }

        public Sending()
        {
            DeparturePoint = "";
            DestinationPoint = "";
            SellsSeatsNumber = 0;
            Train = new PassengerTrain();
        }

        public void SetDeparturePoint()
        {
            Console.WriteLine("Введите место отправления");
            DeparturePoint = Console.ReadLine();
            SetRequiredSeatsNumber();
        }

        public void SetDestinationPoint()
        {
            Console.WriteLine("Введите место назначение");
            DestinationPoint = Console.ReadLine();
            SetRequiredSeatsNumber();
        }

        private void SetRequiredSeatsNumber()
        {
            Random random = new Random();
            if (DeparturePoint != "" && DestinationPoint != "")
                SellsSeatsNumber = random.Next(50, 1000);
        }

        public bool readyForDeparture()
        {
            bool readyForDeparture = false;
            if ((DeparturePoint != "") && (DestinationPoint != "") && (SellsSeatsNumber <= Train.SeatsNumber))
                readyForDeparture = true;
            return readyForDeparture;
        }

        public void ShowInfo()
        {
            Console.WriteLine($"Информация о поезде:\n" +
                $"Пункт Отправления {DeparturePoint}\n" +
                $"Пункт Назначения {DestinationPoint}\n" +
                $"Количество проданых билетов {SellsSeatsNumber}\n" +
                $"Количество мест в поезеде {Train.SeatsNumber}\n" +
                $"Количество вагонов в поезде { Train.Vagons.Length}\n");
        }
    }
}