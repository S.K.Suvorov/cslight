﻿using System;

namespace StringManipulation
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName;
            string middleName;
            string lastName;
            int age;
            string zodiacSign;
            int kidsCount;
            string profession;
            Console.WriteLine("Введите Ваше имя: ");
            firstName=Console.ReadLine();
            Console.WriteLine("Введите Ваше отчество: ");
            middleName = Console.ReadLine();
            Console.WriteLine("Введите Вашу фамилия: ");
            lastName = Console.ReadLine();
            Console.WriteLine("по знаку зодиака Вы: ");
            zodiacSign = Console.ReadLine();
            Console.WriteLine("Введите Ваш возраст: ");
            age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Сколько у Вас детей: ");
            kidsCount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Кем Вы работаете: ");
            profession = Console.ReadLine();
            Console.WriteLine($"Вас зовут {lastName} {firstName} {middleName}. Вам {age}. Вы {zodiacSign}." +
                $" У Вас {kidsCount} детей. По профессии вы {profession} ");
            Console.ReadKey();
        }
    }
}
