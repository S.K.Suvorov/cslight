﻿using System;

namespace DatabaseOfPlayers
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] playersNickName = { "SuperPuper", "Winner", "TrUlaLa" };
            PlayersDataBase playersDataBase = new PlayersDataBase(playersNickName);
            string mainCommand = "";

            while (mainCommand.ToLower() != "выход")
            {
                Console.Clear();
                Console.WriteLine($"Добро пожаловать! Выберите команду из списка:\n\n" +
                    $"Вывести список - вывод списка игроков\n" +
                    $"Добавить Игрока - добавить нового игрока\n" +
                    $"Удалить Игрока  - удалить игрока\n" +
                    $"Ограничить доступ - ограничить доступ игроку по номеру\n" +
                    $"Вернуть доступ - востановить доступ игроку по номеру\n" +
                    $"Выход - выход");

                mainCommand = Console.ReadLine().ToLower();
                switch (mainCommand)
                {
                    case "вывести список":
                        ShowInfo(ref playersDataBase, ref mainCommand);
                        break;
                    case "добавить игрока":
                        playersDataBase.AddInfo();
                        break;
                    case "удалить игрока":
                        playersDataBase.DeleteInfo();
                        break;
                    case "ограничить доступ":
                        playersDataBase.BanPlayer();
                        break;
                    case "вернуть доступ":
                        playersDataBase.UnbanPlayer();
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда.Проверте правильность ввода.Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }
        static void ShowInfo(ref PlayersDataBase playersDataBase, ref string mainCommand)
        {
            string subCommand = "";

            while ((subCommand.ToLower() != "назад") && (subCommand.ToLower() != "выход"))
            {
                Console.Clear();
                playersDataBase.ShowDataBase();
                Console.WriteLine(
                    "\n\nВведите команду:\n" +
                    "Добавить Игрока - добавить нового игрока\n" +
                    "Удалить Игрока  - удалить игрока\n" +
                    "Ограничить доступ - ограничить доступ игроку по номеру\n" +
                    "Вернуть доступ - востановить доступ игроку по номеру\n" +
                    "Назад - для возврата в основное меню\n" +
                    "\nВыход - для выхода \n");
                subCommand = Console.ReadLine().ToLower();
                switch (subCommand)
                {
                    case "выход":
                        mainCommand = "выход";
                        break;
                    case "назад":
                        break;
                    case "удалить игрока":
                        playersDataBase.DeleteInfo();
                        break;
                    case "добавить игрока":
                        playersDataBase.AddInfo();
                        break;
                    case "ограничить доступ":
                        playersDataBase.BanPlayer();
                        break;
                    case "вернуть доступ":
                        playersDataBase.UnbanPlayer();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в выбранное меню");
                        Console.ReadKey();
                        break;
                }
            }
        }
    }
    class Player
    {
        string _nickName;
        int _playerLavel;
        bool _isActive;
        bool _inBanned;

        public Player(string nickName)
        {
            _nickName = nickName;
            _playerLavel = 0;
            _isActive = true;
            _inBanned = false;
        }

        public void PlayerInfo()
        {
            Console.WriteLine("Ник - " + _nickName + "\nУровень игрока - " + _playerLavel);
            if (_isActive)
                Console.WriteLine("В сети");
            else
                Console.WriteLine("Не в сети");
            if (_inBanned)
                Console.WriteLine("Доступ ограничен\n");
            else
                Console.WriteLine("Имеет доступ\n");

        }

        public void PlayerBan()
        {
            if (_inBanned)
                Console.WriteLine("Игрок" + _nickName + "уже имеет данный статус");
            else
                Console.WriteLine("Игроку " + _nickName + " ограничен доступ");
            _inBanned = true;
        }

        public void PlayerUnban()
        {
            if (!_inBanned)
                Console.WriteLine("Игрок" + _nickName + "уже имеет данный статус");
            else
                Console.WriteLine("Игроку " + _nickName + " востановлен доступ");
            _inBanned = false;
        }
    }
    class PlayersDataBase
    {
        Player[] _playersDataBase;

        public PlayersDataBase(Player[] Players)
        {
        }

        public PlayersDataBase(string[] PlayersNickName)
        {
            Player[] tempPlayersDataBase = new Player[PlayersNickName.Length];

            for (int i = 0; i < PlayersNickName.Length; i++)
            {
                tempPlayersDataBase[i] = new Player(PlayersNickName[i]);
            }
            _playersDataBase = tempPlayersDataBase;
        }

        public void ShowDataBase()
        {
            Console.WriteLine("Список игроков:\n");

            for (int i = 0; i < _playersDataBase.Length; i++)
            {
                Console.WriteLine("Игрок №" + (i + 1) + ". ");
                _playersDataBase[i].PlayerInfo();
            }
        }

        public void AddInfo()
        {
            Player[] tempPlayersDataBase = new Player[_playersDataBase.Length + 1];
            string newNick;

            for (int i = 0; i < _playersDataBase.Length; i++)
            {
                tempPlayersDataBase[i] = _playersDataBase[i];
            }

            Console.Clear();
            Console.WriteLine("Введите Ник нового игрока");
            newNick = Console.ReadLine();
            tempPlayersDataBase[_playersDataBase.Length] = new Player(newNick);
            if (ChangeConfirmation())
                _playersDataBase = tempPlayersDataBase;
        }

        private bool ChangeConfirmation()
        {
            string youSure = "";
            bool changeConfirmation = false;
            Console.WriteLine("Вы уверены? Да/Нет");
            youSure = Console.ReadLine();
            switch (youSure.ToLower())
            {
                case "да":
                    Console.WriteLine("Данные изменены. Нажмите Enter для возвращения в основное меню");
                    changeConfirmation = true;
                    Console.ReadKey();
                    break;
                case "нет":
                    Console.WriteLine("Отмена изменений. Нажмите Enter для возвращения в основное меню");
                    changeConfirmation = false;
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Некорректная команда. Проверте правильность ввода. Нажмите Enter для возвращения в основное меню");
                    changeConfirmation = false;
                    Console.ReadKey();
                    break;
            }
            return changeConfirmation;
        }
        private int NumderInput()
        {
            int num = 0;
            bool isNum = false;
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
        public void DeleteInfo()
        {
            int num = -1;
            int shift = 0;
            Player[] tempPlayersDataBase = new Player[_playersDataBase.Length - 1];

            Console.WriteLine($"Введите номер записи для удаления от 1 до {_playersDataBase.Length}:");
            while ((num > (_playersDataBase.Length - 1)) || (num < 0))
            {
                num = NumderInput() - 1;
                if ((num > (_playersDataBase.Length - 1)) || (num < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер записи для удаления от 1 до {_playersDataBase.Length}:");
            }

            for (int i = 0; i < tempPlayersDataBase.Length; i++)
            {
                if (i == num)
                    shift = 1;
                tempPlayersDataBase[i] = _playersDataBase[i + shift];
            }

            if (ChangeConfirmation())
                _playersDataBase = tempPlayersDataBase;
        }
        public void BanPlayer()
        {
            int num = -1;

            Console.Clear();
            Console.WriteLine($"Введите номер порядкового номера игрока от 1 до {_playersDataBase.Length}, которой необходимо ограничить доступ :");
            while ((num > (_playersDataBase.Length - 1)) || (num < 0))
            {
                num = NumderInput() - 1;
                if ((num > (_playersDataBase.Length - 1)) || (num < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер порядкового номера игрока от 1 до {_playersDataBase.Length}, которму необходимо ограничить доступ :");
            }

            _playersDataBase[num].PlayerBan();
            Console.WriteLine("Для возврат в меню нажмите любую кнопку");
            Console.ReadKey();
        }
        public void UnbanPlayer()
        {
            int num = -1;

            Console.Clear();
            Console.WriteLine($"Введите номер порядкового номера игрока от 1 до {_playersDataBase.Length}, которой необходимо вернуть доступ :");
            while ((num > (_playersDataBase.Length - 1)) || (num < 0))
            {
                num = NumderInput() - 1;
                if ((num > (_playersDataBase.Length - 1)) || (num < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер порядкового номера игрока от 1 до {_playersDataBase.Length}, которму необходимо вернуть доступ :");
            }

            _playersDataBase[num].PlayerUnban();
            Console.WriteLine("Для возврат в меню нажмите любую кнопку");
            Console.ReadKey();
        }
    }
}
