﻿using System;

namespace GreatestElement
{
    class Program
    {
        static void Main(string[] args)
        {
            int row = 0;
            int col = 0;
            int greatestElement = int.MinValue;
            int[,] array = new int[10, 10];
            Random random = new Random();

            Console.WriteLine("Исходная матрица:\n");
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array[i, j] = random.Next(0, 100);
                    Console.Write(array[i, j] + " ");
                    if (array[i, j] < 10)
                        Console.Write(" ");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] > greatestElement)
                    {
                        greatestElement = array[i, j];
                        row = i;
                        col = j;
                    }
                }
            }
            Console.WriteLine($"\nМаксимальный элимент матрицы {greatestElement}, находится в строке {row}, столбце {col}");
            array[row, col] = 0;

            Console.WriteLine("\nИзмененная матрица:\n");
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write(array[i, j] + " ");
                    if (array[i, j] < 10)
                        Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }

}
