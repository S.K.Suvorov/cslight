﻿using System;

namespace GladiatorFight
{
    class Program
    {
        static void Main(string[] args)
        {
            Gladiator[] gladiatorList = new Gladiator[8];
            string command = "";
            string LogInfo = "";
            CreatingGladiators(gladiatorList);

            while (command.ToLower() != "выход")
            {
                Console.Clear();
                Console.WriteLine("Сегодня на арене нашего амфитеатра:\n");
                ShowGladiators(gladiatorList);
                Console.WriteLine("Введите команду из списка:\n" +
                    "Турнир - чтобы устроить парные бои для всех(пары выбираются автоматически) \n" +
                    "Бой - чтобы устроить поединок выбранных вами бойцов\n" +
                    "Выход - выйти из программы");

                Console.WriteLine(LogInfo);
                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "турнир":
                        TournamentRound(ref gladiatorList);
                        break;
                    case "бой":
                        CreateDuel(gladiatorList);
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некоректная команда. Нажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }

        static void Fight(Gladiator gladiator1, Gladiator gladiator2)
        {
            Console.Clear();
            Log LogInfo = new Log();

            Console.WriteLine("Первый боец:");
            gladiator1.ShowInfo();
            Console.WriteLine("Второй боец:");
            gladiator2.ShowInfo();
            Console.WriteLine("Нажмите любую кнопку для начала поединка");
            Console.ReadKey();
            Console.Clear();

            while (gladiator1.Health > 0 && gladiator2.Health > 0)
            {
                gladiator1.Action(gladiator2, LogInfo);
                gladiator2.Action(gladiator1, LogInfo);
            }

            LogInfo.GladiatorDieLog(gladiator1, gladiator2);
            LogInfo.ShowLog();
            Console.ReadKey();
        }

        static void TournamentRound(ref Gladiator[] gladiatorList)
        {
            int changeNum;
            Gladiator tempGladiator;
            Random random = new Random();

            for (int i = 0; i < gladiatorList.Length; i++)
            {
                changeNum = random.Next(0, gladiatorList.Length);
                tempGladiator = gladiatorList[changeNum];
                gladiatorList[changeNum] = gladiatorList[i];
                gladiatorList[i] = tempGladiator;
            }


            for (int i = 0; i < (gladiatorList.Length / 2); i++)
                Fight(gladiatorList[i * 2], gladiatorList[i * 2 + 1]);

            DeleteDieGladiators(ref gladiatorList);

            for (int i = 0; i < (gladiatorList.Length); i++)
                gladiatorList[i].ReadyToBattle();
        }

        static void DeleteDieGladiators(ref Gladiator[] gladiatorList)
        {
            Gladiator[] tempGladiatorList;
            int shift;

            if (gladiatorList.Length != 0)
            {
                for (int j = 0; j < gladiatorList.Length; j++)
                {
                    shift = 0;
                    tempGladiatorList = new Gladiator[gladiatorList.Length - 1];

                    if (gladiatorList[j].Health <= 0)
                    {

                        for (int i = 0; i < tempGladiatorList.Length; i++)
                        {
                            if (j == i)
                                shift = 1;
                            tempGladiatorList[i] = gladiatorList[i + shift];
                        }

                        gladiatorList = tempGladiatorList;
                        j = 0;
                    }
                }
            }
        }

        static void CreatingGladiators(Gladiator[] gladiatorList)
        {
            Random random = new Random();

            for (int i = 0; i < gladiatorList.Length; i++)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        gladiatorList[i] = new Provocator(RandomName(), random.Next(14, 17), random.Next(30, 41), random.Next(30, 41));
                        break;
                    case 1:
                        gladiatorList[i] = new Retiarius(RandomName(), random.Next(14, 17), random.Next(30, 41), random.Next(30, 41));
                        break;
                    case 2:
                        gladiatorList[i] = new Veles(RandomName(), random.Next(14, 17), random.Next(30, 41), random.Next(30, 41));
                        break;
                    case 3:
                        gladiatorList[i] = new Secutor(RandomName(), random.Next(14, 17), random.Next(30, 41), random.Next(30, 41));
                        break;
                    case 4:
                        gladiatorList[i] = new Dimachaerus(RandomName(), random.Next(14, 17), random.Next(30, 41), random.Next(30, 41));
                        break;
                }
            }
        }

        static void CreateDuel(Gladiator[] gladiatorList)
        {
            Gladiator gladiator1;
            Gladiator gladiator2;

            Console.WriteLine("Выберите бойцов.\nВведите номер первого бойца:");
            gladiator1 = ChoiceOfFighters(gladiatorList);
            Console.WriteLine("Первый боец выбран\nВведите номер второго бойца:");
            gladiator2 = ChoiceOfFighters(gladiatorList);

            while (gladiator1 == gladiator2)
            {
                Console.WriteLine("Боец не может сражаться сам с собой.\nВведите номер второго бойца:");
                gladiator2 = ChoiceOfFighters(gladiatorList);
            }
            Console.Clear();

            Fight(gladiator1, gladiator2);

            if (gladiator1.Health > 0)
                gladiator1.ReadyToBattle();
            if (gladiator2.Health > 0)
                gladiator2.ReadyToBattle();

            DeleteDieGladiators(ref gladiatorList);
        }

        static Gladiator ChoiceOfFighters(Gladiator[] gladiatorList)
        {
            int numOfFighter = -1;
            while ((numOfFighter > (gladiatorList.Length - 1)) || (numOfFighter < 0))
            {
                numOfFighter = NumderInput() - 1;
                if ((numOfFighter > (gladiatorList.Length - 1)) || (numOfFighter < 0))
                    Console.WriteLine($"Такой номер отсутствует.\n Введите номер записи для удаления от 1 до {gladiatorList.Length}:");
            }
            return gladiatorList[numOfFighter];
        }

        static int NumderInput()
        {
            int num = 0;
            bool isNum = false;

            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);

                if (isNum != true)
                    Console.WriteLine("Некорректный ввод данных. Введите число");
            }

            return num;
        }

        static void ShowGladiators(Gladiator[] gladiatorList)
        {
            for (int i = 0; i < gladiatorList.Length; i++)
            {
                Console.WriteLine($"Гладиатор №{i + 1}");
                gladiatorList[i].ShowInfo();
            }
        }

        static string RandomName()
        {
            Random random = new Random();
            string name = "";

            switch (random.Next(0, 10))
            {
                case 0:
                    name = "Max";
                    break;
                case 1:
                    name = "Dan";
                    break;
                case 2:
                    name = "Din";
                    break;
                case 3:
                    name = "Ben";
                    break;
                case 4:
                    name = "Tom";
                    break;
                case 5:
                    name = "Jack";
                    break;
                case 6:
                    name = "Fred";
                    break;
                case 7:
                    name = "Jhon";
                    break;
                case 8:
                    name = "Jhim";
                    break;
                case 9:
                    name = "Sam";
                    break;
            }

            return name;
        }
    }

    class Log
    {
        private string _infoLog;

        public Log()
        {
            _infoLog = "";
        }

        public void GladiatorDieLog(Gladiator gladiator1, Gladiator gladiator2)
        {
            if (gladiator1.Health <= 0 && gladiator2.Health <= 0)
                _infoLog += $"Ничья, оба гладиатора валяются в лужах собственной крови.";
            else if (gladiator1.Health <= 0 && gladiator2.Health > 0)
                _infoLog += $"{gladiator2.Name} победил. Тело его противника бьется в предсмертной агонии";
            else
                _infoLog += $"{gladiator1.Name} победил. Тело его противника бьется в предсмертной агонии";
        }

        public void ShowLog()
        {
            Console.WriteLine(_infoLog);
        }

        public void StartAttackLog(Gladiator Gladiator, Gladiator Enemy)
        {
            _infoLog += $"{ Gladiator.Name } атакует своего противника {Enemy.Name}";
        }

        public void UnSuccessfulAttackLog()
        {
            _infoLog += $", но не попадает по нему\n";
        }

        public void SuccessfulDefenceLog(Gladiator Gladiator)
        {
            _infoLog += $", но {  Gladiator.Name } блокирует удар\n";
        }

        public void UnSuccessfulDefenceLog()
        {
            _infoLog += $" и попадает по нему. ";
        }

        public void TakeHitLog(Gladiator Gladiator, int Damage)
        {
            _infoLog += $"{ Gladiator.Name } получает {Damage} урона. У него осталось {Gladiator.Health} здоровья\n";
        }

        public void TakeDartHitLog(bool DefenceResult)
        {
            if (!DefenceResult)
            {
                _infoLog += $" и тот  получает серьезную рану\n";
            }
            else
                _infoLog += " и попадает по нему. Дротик застревает в снаряжение противника, мешая ему двигаться\n";
        }

        public void ChangeStatusLog(Gladiator Gladiator, string NewStatus)
        {
            switch (NewStatus)
            {
                case "Stan":
                    _infoLog += $"{ Gladiator.Name } оглушен, и не может атаковать\n";
                    break;
                case "Tangled In Net":
                    _infoLog += $" и попадает по нему\n{ Gladiator.Name } запутался в сети,упал на землю и не может встать\n";
                    break;
                case "Ready To Act":
                    if (Gladiator.Status == "Tangled In Net")
                        _infoLog += ($"{ Gladiator.Name } сорвал с себя сеть, и готов сражатся дальше\n");
                    if (Gladiator.Status == "Stan")
                        _infoLog += ($"{ Gladiator.Name } пришел в себя, и готов сражатся дальше\n");
                    break;
            }
        }

        public void ClassSkillLog(Provocator Gladiator)
        {
            _infoLog += $"{ Gladiator.Name } наносит обманный удар своему противнику, заставляя его открыться\n";
        }

        public void ClassSkillLog(Retiarius Gladiator, Gladiator Enemy)
        {
            if (Gladiator.NetAvailable)
                _infoLog += $"{ Gladiator.Name } кидает сетью в противника {Enemy.Name}";
            else
                _infoLog += $"{ Gladiator.Name } поднял сеть с поля\n";
        }

        public void ClassSkillLog(Veles Gladiator, Gladiator Enemy)
        {
            _infoLog += $"{ Gladiator.Name } кидает дротиком в противника {Enemy.Name}";
        }

        public void ClassSkillLog(Secutor Gladiator, Gladiator Enemy)
        {
            _infoLog += $"{ Gladiator.Name } бьет щитом {Enemy.Name} ";
        }
    }

    abstract class Gladiator
    {
        public string Name { get; private set; }
        public string ClassDescription { get; private set; }
        public int Health { get; private set; }
        public int MaxHealth { get; private set; }
        public int MaxWeaponDamage { get; private set; }
        public int Armor { get; private set; }
        public int Agility { get; private set; }
        public int Strength { get; private set; }
        public int BonusAgility { get; protected set; }
        public int StunRound { get; private set; }
        public bool IsReadyToAct { get; private set; }
        public string Status { get; private set; }

        public Gladiator(string name, int maxHealth, int maxWeaponDamage, int armor, int agility, int strenght)
        {
            Name = name;
            Health = maxHealth;
            MaxHealth = maxHealth;
            MaxWeaponDamage = maxWeaponDamage;
            Armor = armor;
            Agility = agility;
            Strength = strenght;
            IsReadyToAct = true;
            Status = "";
        }

        public void ChangeGladiatorStatus(Log InfoLog, string NewStatus)
        {
            switch (NewStatus)
            {
                case "Stan":
                    if (!Defence())
                    {
                        Random random = new Random();
                        StunRound = random.Next(1, 4);
                        IsReadyToAct = false;
                        InfoLog.ChangeStatusLog(this, NewStatus);
                        Status = "Stan";
                    }
                    else
                        InfoLog.SuccessfulDefenceLog(this);
                    break;
                case "Tangled In Net":
                    if (!Defence())
                    {
                        IsReadyToAct = false;
                        InfoLog.ChangeStatusLog(this, NewStatus);
                        Status = "Tangled In Net";
                    }
                    else
                        InfoLog.SuccessfulDefenceLog(this);
                    break;
                case "Ready To Act":
                    IsReadyToAct = true;
                    InfoLog.ChangeStatusLog(this, NewStatus);
                    Status = "";
                    break;
            }
        }

        public void TakeDartAttackHit(int Damage, Log InfoLog)
        {
            Random random = new Random();

            bool DefenceResult = Defence();
            InfoLog.TakeDartHitLog(DefenceResult);
            if (!DefenceResult)
            {
                Health -= Damage;
                InfoLog.TakeHitLog(this, Damage);
            }
            else
                AgilityDeBuff(random.Next(1, 10)); ;
        }

        public void ShowInfo()
        {
            Console.WriteLine($"{Name}. {ClassDescription}\n" +
                $"Характеристики:\n" +
                $"Очки здоровья - {Health}. Ловкость - {Agility}. Сила - {Strength}. Броня - {Armor}. Урон оружием - {MaxWeaponDamage}. \n");
        }

        public void ReadyToBattle()
        {
            StunRound = 0;
            BonusAgility = 0;
            IsReadyToAct = true;
            if ((MaxHealth - Health) > 5)
                Health += 5;
            else
                Health = MaxHealth;
            ClassReadyToBattle();
        }

        protected void SetClassDescription(string classDescription)
        {
            ClassDescription = classDescription;
        }

        public void Attack(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();
            int AttackRoll = random.Next(1, 100);
            int damage;
            InfoLog.StartAttackLog(this, Enemy);

            if (AttackRoll <= (Agility + BonusAgility - Armor / 10))
            {
                damage = ((Strength / 10) + random.Next(1, MaxWeaponDamage)) * (100 - Enemy.Armor) / 100;
                Enemy.TakeDamage(damage, InfoLog);
            }
            else
            {
                InfoLog.UnSuccessfulAttackLog();
            }
        }

        public bool Defence(Log InfoLog)
        {
            bool DefenceIsDone = false;
            Random random = new Random();
            int DefenceRoll = random.Next(1, 100);
            int ChanceOfDefence;


            if (IsReadyToAct)
                ChanceOfDefence = Agility + BonusAgility - Armor / 10;
            else
                ChanceOfDefence = 5;

            if (DefenceRoll <= ChanceOfDefence)
                DefenceIsDone = true;
            if (DefenceIsDone)
                InfoLog.SuccessfulDefenceLog(this);
            if (!DefenceIsDone)
                InfoLog.UnSuccessfulDefenceLog();
            return DefenceIsDone;
        }

        public bool Defence()
        {
            bool DefenceIsDone = false;
            Random random = new Random();
            int DefenceRoll = random.Next(1, 100);
            int ChanceOfDefence;

            if (IsReadyToAct)
                ChanceOfDefence = Agility + BonusAgility - Armor / 10;
            else
                ChanceOfDefence = 5;

            if (DefenceRoll <= ChanceOfDefence)
                DefenceIsDone = true;

            return DefenceIsDone;
        }

        public void TakeDamage(int Damage, Log InfoLog)
        {
            if (!Defence(InfoLog))
            {
                Health -= Damage;
                InfoLog.TakeHitLog(this, Damage);
            }
        }

        public void WhileGladiatorImmobilize(Log InfoLog)
        {
            if (StunRound == 0)
            {
                Random random = new Random();
                int StrenghtRoll = random.Next(1, 100);
                if (StrenghtRoll <= Strength - Armor / 10)
                    IsReadyToAct = true;

            }
            else
            {
                StunRound--;
                if (StunRound <= 0)
                    IsReadyToAct = true;
            }

            if (IsReadyToAct)
                InfoLog.ChangeStatusLog(this, "Ready To Act");
        }

        public void AgilityDeBuff(int AgilityDeBuff)
        {
            BonusAgility -= AgilityDeBuff;
        }

        public void Action(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();

            if (Health > 0)
            {
                if (IsReadyToAct)
                {
                    switch (random.Next(1, 4))
                    {
                        case 1:
                        case 2:
                            Attack(Enemy, InfoLog);
                            break;
                        case 3:
                            ClassSkill(Enemy, InfoLog);
                            break;
                    }
                }
                else
                    WhileGladiatorImmobilize(InfoLog);
            }
        }

        public abstract void ClassSkill(Gladiator Enemy, Log InfoLog);
        public abstract void ClassReadyToBattle();
    }

    class Provocator : Gladiator
    {
        public Provocator(string name, int health, int agility, int strenght) : base(name, health, 12, 25, agility, strenght)
        {
            SetClassDescription("Провокатор. Среднебронированный воин, вооруженный мечом. Особое умение - Финт - удар, после обманной атаки, поподающей с большей вероятностью в цель");
        }

        public override void ClassSkill(Gladiator Enemy, Log InfoLog)
        {
            Feint(Enemy, InfoLog);
        }

        public void Feint(Gladiator Enemy, Log InfoLog)
        {
            InfoLog.ClassSkillLog(this);
            BonusAgility = 10;
            Attack(Enemy, InfoLog);
            BonusAgility = 0;
        }

        public override void ClassReadyToBattle()
        {

        }
    }

    class Retiarius : Gladiator
    {
        public bool NetAvailable { get; private set; }

        public Retiarius(string name, int health, int agility, int strenght) : base(name, health, 15, 10, agility, strenght)
        {
            SetClassDescription("Ретиарий. Легкобронированый боец с сеткой и трезубцем. Особое умение - бросок сетью, при попадании запутывающей противника");
            NetAvailable = true;
        }

        public override void ClassSkill(Gladiator Enemy, Log InfoLog)
        {
            if (NetAvailable && Enemy.IsReadyToAct)
            {
                InfoLog.ClassSkillLog(this, Enemy);
                ThrowNet(Enemy, InfoLog);
            }
            else if (Enemy.IsReadyToAct)
            {
                InfoLog.ClassSkillLog(this, Enemy);
                ReturningNet(InfoLog);
            }
            else if (!Enemy.IsReadyToAct)
                Attack(Enemy, InfoLog);
        }

        private void ThrowNet(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();
            int AttackRoll = random.Next(1, 100);

            if (AttackRoll <= (Agility + BonusAgility - Armor / 10))
            {
                Enemy.ChangeGladiatorStatus(InfoLog, "Tangled In Net");
            }
            else
            {
                InfoLog.UnSuccessfulAttackLog();
            }

            NetAvailable = false;
        }

        private void ReturningNet(Log InfoLog)
        {
            NetAvailable = true;
        }

        public override void ClassReadyToBattle()
        {
            NetAvailable = true;
        }
    }

    class Veles : Gladiator
    {
        private int _dartsCount = 5;

        public Veles(string name, int health, int agility, int strenght) : base(name, health, 12, 10, agility, strenght)
        {
            SetClassDescription("Велит. Легкобронированый боец с дротиками. Особое умение - бросок дротиком, наносящий серьезные раны, при попадание в броню застревает в ней и мешает движению");
        }

        public override void ClassSkill(Gladiator Enemy, Log InfoLog)
        {
            if (_dartsCount > 1)
                ThrowDart(Enemy, InfoLog);
            else
                Attack(Enemy, InfoLog);
        }

        private void ThrowDart(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();
            int AttackRoll = random.Next(1, 100);
            InfoLog.ClassSkillLog(this, Enemy);

            if (AttackRoll <= (Agility + BonusAgility - Armor / 10))
            {
                int damage = ((Strength / 10) + random.Next(MaxWeaponDamage / 2, MaxWeaponDamage));
                Enemy.TakeDartAttackHit(damage, InfoLog);
            }
            else
                InfoLog.UnSuccessfulAttackLog();
        }

        public override void ClassReadyToBattle()
        {
            _dartsCount = 5;
        }
    }

    class Secutor : Gladiator
    {
        public Secutor(string name, int health, int agility, int strenght) : base(name, health, 12, 35, agility, strenght)
        {
            SetClassDescription("Секутор. Тяжелобронированый боец вооруженный мечом. Особое умение - удар тяжелым щитом, оглушающий противника");
        }

        public override void ClassSkill(Gladiator Enemy, Log InfoLog)
        {
            if (Enemy.IsReadyToAct)
                HitByShield(Enemy, InfoLog);
            else
                Attack(Enemy, InfoLog);
        }

        public void HitByShield(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();
            int AttackRoll = random.Next(1, 100);
            InfoLog.ClassSkillLog(this, Enemy);

            if (AttackRoll <= (Agility + BonusAgility - Armor / 10))
                Enemy.ChangeGladiatorStatus(InfoLog, "Stan");

            else
                InfoLog.UnSuccessfulAttackLog();
        }

        public override void ClassReadyToBattle()
        {

        }
    }

    class Dimachaerus : Gladiator
    {
        public Dimachaerus(string name, int health, int agility, int strenght) : base(name, health, 10, 30, agility, strenght)
        {
            SetClassDescription("Димахер. Среднебронированый боец вооруженный 2 саблями. Особое умение - шквал ударов по противнику");
        }

        public override void ClassSkill(Gladiator Enemy, Log InfoLog)
        {
            FlurryOfBlows(Enemy, InfoLog);
        }

        public void FlurryOfBlows(Gladiator Enemy, Log InfoLog)
        {
            Random random = new Random();
            int attackCount = random.Next(2, 5);
            for (int i = 0; i < attackCount; i++)
                Attack(Enemy, InfoLog);
        }

        public override void ClassReadyToBattle()
        {

        }
    }
}
