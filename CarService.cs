﻿using System;
using System.Collections.Generic;

namespace CarService
{
    class Program
    {
        static void Main(string[] args)
        {

            CarService carService = new CarService(new Warehouse(new List<WarehouseCell> { new WarehouseCell("Масло", 5, 100), new WarehouseCell("Колесо", 5, 500), new WarehouseCell("Свечи", 5, 100), new WarehouseCell("Выхлопная труба", 5, 300), }), 500);
            int commandCode = 0;

            carService.SetBrokenCar(SetCarTypeName());

            while (commandCode != 5 && carService.Money >= 0)
            {
                Console.Clear();
                Console.SetCursorPosition(0, 1);
                carService.ShowCarServiceInfo();
                Console.SetCursorPosition(0, 10);
                Console.WriteLine($"Введите код команды из списка:" +
                    $"\n1 - выбрать деталь со склада для замены" +
                    $"\n2 - купить детали на склад" +
                    $"\n3 - заменить в машине деталь на выбранную" +
                    $"\n4 - отказать в ремонте" +
                    $"\n5 - выход из программы");

                commandCode = NumderInput();

                switch (commandCode)
                {
                    case 1:
                        ChoseCarParts(carService);
                        break;
                    case 2:
                        BuyParts(carService);
                        break;
                    case 3:
                        FixBrokenCar(carService);
                        break;
                    case 4:
                        RefuseService(carService);
                        break;
                    case 5:
                        break;
                    default:
                        Console.WriteLine("Некорректная команда. Нажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        break;
                }
            }

            if (carService.Money < 0)
            {
                Console.Clear();
                Console.WriteLine("Вы банкрот");
            }

        }

        static void RefuseService(CarService carService)
        {
            Console.Clear();
            carService.MonetaryFine();
            carService.SetBrokenCar(SetCarTypeName());
            Console.WriteLine($"Вы отказались в обслуживании машины, и заплатили штраф в размере 300 $" +
                $"\nНажмите любую кнопку для возврат в меню");
        }

        static void FixBrokenCar(CarService carService)
        {
            int tempMoney = carService.Money;
            bool fixResult;

            Console.Clear();
            if (carService.PartForFix != null)
            {
                fixResult = carService.FixCar();
                if ((fixResult))
                {
                    carService.SetBrokenCar(SetCarTypeName());
                    Console.WriteLine($"Вы починили машину, и получили {carService.Money - tempMoney} $" +
                        $"\nНажмите любую кнопку для возврат в меню");
                }
                else if (!fixResult)
                {
                    carService.SetBrokenCar(SetCarTypeName());
                    Console.WriteLine($"Вы не починили машину, и возместили ущерб в размере {tempMoney - carService.Money} $" +
                        $"\nНажмите любую кнопку для возврат в меню");
                }
            }
            else
                Console.WriteLine("Сначала выберите деталь для замены.Нажмите любую кнопку для возврат в меню");

            Console.ReadKey();
        }

        static int NumderInput()
        {
            int num = 0;
            bool isNum = false;

            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);

                if (isNum != true)
                    Console.WriteLine("Некорректный ввод данных. Введите число");
            }

            return num;
        }

        static void ChoseCarParts(CarService carService)
        {
            int partCode = -1;

            Console.Clear();
            carService.Warehouse.ShowWareHouseInfo();

            Console.WriteLine($" Введите код детали, которую хотите использовать для ремонта:");
            while (partCode < 0 || partCode >= carService.Warehouse.WarehouseSize())
            {
                partCode = NumderInput();
                if (partCode < 0 || partCode >= carService.Warehouse.WarehouseSize())
                    Console.WriteLine($"Такой код отсутствует.\n Введите код детали, которую хотите использовать для ремонта:");
            }

            if (carService.Warehouse.GetPartCount(partCode) > 0)
                carService.ChosePartForFix(partCode);
        }

        static void BuyParts(CarService carService)
        {
            int partCode = -1;
            int partCount;

            Console.Clear();
            carService.Warehouse.ShowWareHouseInfo();

            Console.WriteLine($" Введите код детали, которую хотите купить:");
            while (partCode < 0 || partCode >= carService.Warehouse.WarehouseSize())
            {
                partCode = NumderInput();
                if (partCode < 0 || partCode >= carService.Warehouse.WarehouseSize())
                    Console.WriteLine($"Такой код отсутствует.\n Введите код детали, которую хотите купить:");
            }

            Console.WriteLine($" Введите количество деталей, которое хотите купить:");
            partCount = NumderInput();
            while (carService.Warehouse.GetPartCost(partCode) * partCount > carService.Money)
            {
                Console.WriteLine($"У Вас не хватает денег на покупку такого количества деталей." +
                    $"\n Введите количество деталей, которое хотите купить. Вы можете себе позволить до {carService.Money / carService.Warehouse.GetPartCost(partCode)}:");
                partCount = NumderInput();
            }
            carService.BuyParts(partCode, partCount);

        }

        static string SetCarTypeName()
        {
            string carTypeName = "";
            Random random = new Random();

            switch (random.Next(0, 4))
            {
                case 0:
                    carTypeName = "Ford";
                    break;
                case 1:
                    carTypeName = "Audi";
                    break;
                case 2:
                    carTypeName = "BMW";
                    break;
                case 3:
                    carTypeName = "OPEL";
                    break;
            }
            return carTypeName;
        }
    }

    class MachinePart
    {
        public string MachinePartName { get; private set; }
        public int MachinePartCost { get; private set; }
        public bool IsBroken { get; private set; }

        public MachinePart(string machinePartName, int machinePartCost)
        {
            MachinePartName = machinePartName;
            MachinePartCost = machinePartCost;
            IsBroken = false;
        }

        public void FixPart()
        {
            IsBroken = false;
        }

        public void BreakPart()
        {
            IsBroken = true;
        }

    }

    class WarehouseCell
    {
        public MachinePart MachinePart;
        public int MachinePartCount { get; private set; }

        public WarehouseCell(string machinePartName, int machinePartCount, int machinePartCost)
        {
            MachinePart = new MachinePart(machinePartName, machinePartCost);
            MachinePartCount = machinePartCount;
        }

        public MachinePart ChoseCarPart()
        {
            MachinePart chosenPart;
            chosenPart = MachinePart;
            MachinePartCount--;
            return chosenPart;
        }

        public void IncreasedPartCount(int machinePartCount)
        {
            MachinePartCount += machinePartCount;
        }
    }

    class Warehouse
    {
        private List<WarehouseCell> _machineParts;

        public Warehouse(List<WarehouseCell> machineParts)
        {
            _machineParts = machineParts;
        }

        public int GetPartCost(int partCode)
        {
            int partCost = _machineParts[partCode].MachinePart.MachinePartCost;
            return partCost;
        }

        public string GetPartName(int partCode)
        {
            string PartName = _machineParts[partCode].MachinePart.MachinePartName;
            return PartName;
        }

        public int GetPartCount(int partCode)
        {
            int partCount = _machineParts[partCode].MachinePartCount;
            return partCount;
        }

        public int WarehouseSize()
        {
            int WarehouseSize = _machineParts.Count;
            return WarehouseSize;
        }

        public void GiveBackPart(int partCode)
        {
            _machineParts[partCode].IncreasedPartCount(1);
        }

        public void ShowWareHouseInfo()
        {
            for (int i = 0; i < _machineParts.Count; i++)
                Console.WriteLine($"{i} - {_machineParts[i].MachinePart.MachinePartName}, в наличие на складе {_machineParts[i].MachinePartCount}, стоимость за штуку {_machineParts[i].MachinePart.MachinePartCost}");
        }

        public void BuyParts(int PartCode, int PartCount)
        {
            _machineParts[PartCode].IncreasedPartCount(PartCount);
        }

        public MachinePart ChoseCarPart(int PartCode)
        {
            MachinePart chosenPart = _machineParts[PartCode].ChoseCarPart();
            return chosenPart;
        }
    }

    class Car
    {
        public string CarName { get; private set; }
        public List<MachinePart> MachinePartsDataBase { get; private set; }

        public Car(string carName)
        {
            CarName = carName;
            SetRandomMachinePart();
        }

        public Car(string carName, List<MachinePart> machinePartsDataBase)
        {
            CarName = carName;
            MachinePartsDataBase = machinePartsDataBase;
        }

        public void FixCar(MachinePart NewPart)
        {
            for (int i = 0; i < MachinePartsDataBase.Count; i++)
            {
                if (ShowBrokenPart().MachinePartName == NewPart.MachinePartName)
                {
                    ShowBrokenPart().FixPart();
                    break;
                }
            }
        }

        private void SetRandomMachinePart()
        {
            Random random = new Random();

            MachinePartsDataBase[random.Next(0, MachinePartsDataBase.Count)].BreakPart();
        }

        public MachinePart ShowBrokenPart()
        {
            for (int i = 0; i < MachinePartsDataBase.Count; i++)
            {
                if (MachinePartsDataBase[i].IsBroken)
                    return MachinePartsDataBase[i];
            }
            return null;
        }

        public void ShowMachinePart()
        {
            for (int i = 0; i < MachinePartsDataBase.Count; i++)
            {
                Console.Write(MachinePartsDataBase[i].MachinePartName + "  ");
                if (MachinePartsDataBase[i].IsBroken)
                    Console.WriteLine("Сломано");
                else
                    Console.WriteLine("Работает");
            }

        }
    }

    class CarService
    {
        public Warehouse Warehouse { get; private set; }
        public int Money { get; private set; }
        public Car BrokenCar { get; private set; }
        public MachinePart PartForFix { get; private set; }

        public CarService(Warehouse warehouse, int money)
        {
            Warehouse = warehouse;
            Money = money;
        }

        public void ShowCarServiceInfo()
        {
            string info = "";
            if (PartForFix != null)
                info = PartForFix.MachinePartName;

            Console.WriteLine($"Количество денег - {Money} $\n" +
                $"Машина на ремонте - {BrokenCar.CarName}\n" +
                $"Перечень деталей:");
            BrokenCar.ShowMachinePart();
            Console.WriteLine($"Деталь выбранная для замены - {info} ");
        }

        public void BuyParts(int PartCode, int PartCount)
        {
            Warehouse.BuyParts(PartCode, PartCount);
            Money -= Warehouse.GetPartCost(PartCode) * PartCount;
        }

        public void ChosePartForFix(int PartCode)
        {
            if (PartForFix != null)
            {
                for (int i = 0; i < Warehouse.WarehouseSize(); i++)
                {
                    if (Warehouse.GetPartName(i) == PartForFix.MachinePartName)
                        Warehouse.GiveBackPart(i);
                }
            }
            PartForFix = Warehouse.ChoseCarPart(PartCode);
        }

        public bool FixCar()
        {
            bool fixResult = false;

            BrokenCar.FixCar(PartForFix);
            if (PartForFix != null)
            {
                if (BrokenCar.ShowBrokenPart() == null)
                    fixResult = true;

                if (fixResult)
                    Payment();
                else
                    RepairTheDamage();

                PartForFix = null;
            }
            return fixResult;
        }

        public void Payment()
        {
            int payForWork = PartForFix.MachinePartCost;
            int pay = PartForFix.MachinePartCost + payForWork;
            Money += pay;
        }

        public void MonetaryFine()
        {
            int fine = 300;
            Money -= fine;
        }

        public void RepairTheDamage()
        {
            int payForWork = PartForFix.MachinePartCost;
            int repairTheDamage = -(PartForFix.MachinePartCost + payForWork);
            Money += repairTheDamage;
        }

        public void SetBrokenCar(string CarName)
        {
            List<MachinePart> machinePartsDataBase = new List<MachinePart> { new MachinePart("Масло", 100), new MachinePart("Колесо", 500), new MachinePart("Свечи", 100), new MachinePart("Выхлопная труба", 300), };
            Random random = new Random();
            machinePartsDataBase[random.Next(0, machinePartsDataBase.Count)].BreakPart();
            BrokenCar = new Car(CarName, machinePartsDataBase);
        }
    }
}
