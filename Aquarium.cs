﻿using System;
using System.Collections.Generic;

namespace Aquarium
{
    class Program
    {
        static void Main(string[] args)
        {
            FishesDataBase fishesDataBase = new FishesDataBase(new List<Fish> { new Fish("Золотая рыбка"), new Fish("Меченосец"), new Fish("Гуппи"), new Fish("Астронотус") });
            Aquarium aquarium = new Aquarium(new List<Fish> { new Fish("Золотая рыбка"), new Fish("Меченосец"), new Fish("Гуппи"), new Fish("Астронотус"), new Fish("Гуппи"), new Fish("Гуппи") }, 8);
            string command = "";

            while (command.ToLower() != "выход")
            {
                Console.Clear();
                Console.SetCursorPosition(0, 1);
                aquarium.ShowInfo();
                Console.SetCursorPosition(0, 1 + aquarium.MaxFishesCount);
                Console.WriteLine("Нажмите Enter для пропуска времени, или введите команду из списка: \n" +
                    "Добавить - добавить рыбку в Аквариум\n" +
                    "Достать - достать рыбку из Аквариума\n" +
                    "Выход - выйти из программы");
                command = Console.ReadLine().ToLower();

                switch (command)
                {
                    case "":
                        aquarium.CheckingForFishSurvivability();
                        break;
                    case "добавить":
                        Console.Clear();
                        if(aquarium.MaxFishesCount>aquarium.Fishes.Count)
                        {
                            Console.WriteLine("Введите номер из списка, соответсвующий типу, который вы хотите добавить в аквариум:");
                            fishesDataBase.ShowInfo();
                            int num = -1;
                            while (num < 0 || num > fishesDataBase.FishesTypeList.Count)
                            {
                                num = NumberInput() - 1;
                                if (num < 0 || num > (fishesDataBase.FishesTypeList.Count-1))
                                    Console.WriteLine($"Такой рыбки нет.Введите номер рыбки из списка, которую вы хотите достать из аквариума, от 1 до {fishesDataBase.FishesTypeList.Count} :");
                            }
                            aquarium.Fishes.Add(fishesDataBase.FishesTypeList[num]);
                        }
                        else
                            Console.WriteLine("Больше рыбок в аквариуме не разместить");
                        Console.WriteLine("Нажмите любую кнопку для продолжения");
                        Console.ReadKey();
                        break;
                    case "достать":
                        aquarium.RemoveFish();
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некорректный ввод данных.Нажмите любую кнопку для возврата в меню");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }

        static int NumberInput()
        {
            int num = 0;
            bool isNum = false;
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
    }

    class Fish
    {
        public string FishTypeName { get; private set; }
        public int FishAge { get; private set; }
        public bool IsLife { get; private set; }

        public Fish(string fishTypeName)
        {
            FishTypeName = fishTypeName;
            FishAge = 0;
            IsLife = true;
        }

        public void CheckingForFishSurvivability()
        {
            Random random = new Random();
            float dieChance = FishAge / (random.Next(1, (5 + FishAge)));

            if (dieChance >= 1)
                IsLife = false;
            else if (IsLife)
                FishAge++;
        }

        public void ShowInfo()
        {
            if (IsLife)
                Console.Write($"{FishTypeName}, {FishAge} лет, жива\n");
            else
                Console.Write($"{FishTypeName}, {FishAge} лет, умерла\n");
        }
    }

    class FishesDataBase
    {
        public List<Fish> FishesTypeList { get; private set; }

        public FishesDataBase(List<Fish> fishesTypeList)
        {
            FishesTypeList = fishesTypeList;
        }

        public void ShowInfo()
        {
            for (int i = 0; i < FishesTypeList.Count; i++)
            {
                Console.WriteLine($"Рыбка № {i + 1}  { FishesTypeList[i].FishTypeName}");
            }
        }
    }

    class Aquarium
    {
        public List<Fish> Fishes { get; private set; }
        public int MaxFishesCount { get; private set; }

        public Aquarium(List<Fish> fishes, int maxFishesCount)
        {
            Fishes = fishes;
            MaxFishesCount = maxFishesCount;
        }

        public void ShowInfo()
        {
            for (int i = 0; i < Fishes.Count; i++)
            {
                Console.Write($"Рыбка № {i + 1} ");
                Fishes[i].ShowInfo();
            }
        }

        public void RemoveFish()
        {
            int fishNum = -1;
            Console.WriteLine("Введите номер рыбки из списка, которую вы хотите достать из аквариума:");
            while (fishNum < 0 || fishNum > (Fishes.Count - 1))
            {
                fishNum = NumberInput() - 1;
                if (fishNum < 0 || fishNum > (Fishes.Count-1))
                    Console.WriteLine($"Такой рыбки нет.Введите номер рыбки из списка, которую вы хотите достать из аквариума, от 1 до {Fishes.Count} :");
            }
            Fishes.Remove(Fishes[fishNum]);
            Console.WriteLine("Рыбка убрана из аквариума");
        }

        private void RemoveDiedFihes()
        {
            for (int i = (Fishes.Count-1); i >=0; i--)
            {
                if (!Fishes[i].IsLife)
                    Fishes.Remove(Fishes[i]);
               
            }
        }

        public void AddFish(Fish fish)
        {
            Fishes.Add(fish);

        }

        public void CheckingForFishSurvivability()
        {
            int dieFish = 0;
            for (int i = 0; i < Fishes.Count; i++)
            {
                Fishes[i].CheckingForFishSurvivability();
                if (!Fishes[i].IsLife)
                    dieFish++;
            }

            if (dieFish > 0)
            {
                Console.Clear();
                Console.SetCursorPosition(0, 1);
                ShowInfo();
                Console.WriteLine($" \n\n Умерло { dieFish } рыбок.\n Мертвые рыбки будут убраны из аквариума.\n Нажмите любую кнопку для продолжения");
                Console.ReadKey();
                RemoveDiedFihes();
            }
        }

        private int NumberInput()
        {
            int num = 0;
            bool isNum = false;
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
    }
}
