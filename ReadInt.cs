﻿using System;

namespace ReadInt
{
    class Program
    {
        static void Main(string[] args)
        {

            int num;
           
            Console.WriteLine("Введите число ");
            num = NumderInput();
            Console.Clear();
            Console.WriteLine("Было введено число "+ num);
            
        }

        static int NumderInput()
        {
            int num=0;
            bool isNum=false;

            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
    }
}
