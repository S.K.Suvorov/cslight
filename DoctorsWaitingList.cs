﻿using System;

namespace DoctorsWaitingList
{
    class DoctorsWaitingListProgramm
    {
        static void Main()
        {
            int oldLadyCount;
            int timeForOnePeople = 10;
            int hoursOfWaiting;
            int minutesOfWaiting;
            Console.Write("Ведите количество старушек в очереди - ");
            oldLadyCount = Convert.ToInt32(Console.ReadLine());
            hoursOfWaiting = oldLadyCount * timeForOnePeople / 60;
            minutesOfWaiting = oldLadyCount * timeForOnePeople % 60;
            Console.WriteLine($"Количество старушек в очереди перед Вами - {oldLadyCount}шт, Вы проведете в очереди - {hoursOfWaiting} часа " +
                $"и {minutesOfWaiting} минуты");
            Console.ReadKey();
        }
    }
}
