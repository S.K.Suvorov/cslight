﻿using System;

namespace CurrencyConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            float redPaperCount =100;
            float bluePaperCount =1000;
            float yellowPaperCount =10000;
            float blueToRed = 9.5f;
            float yellowToRed = 99.75f;
            float yellowToBlue = 10.5f;
            int userCommand=0;
            float moneyForChange;

            while (userCommand!=7)
            {
                Console.WriteLine($"Ваш баланс: \n Красных бумажек {redPaperCount} \n Синих бумажек {bluePaperCount} \n Желтых бумажек {yellowPaperCount} \n\n" +
                $"Курс: \n  {blueToRed} Синих=>Красная\n {yellowToBlue} Желтых=>Синия \n{yellowToRed} Желтых=>Красная \n \n" +
                $"Выберите необходимое действие:\n" +
                $"Введите 1 если хотите конвертирова Красные бумажки в Синие бумажки по установленному курсу\n" +
                $"Введите 2 если хотите конвертирова Красные бумажки в Желтые бумажки по установленному курсу\n" +
                $"Введите 3 если хотите конвертирова Синие бумажки в Красные бумажки по установленному курсу\n" +
                $"Введите 4 если хотите конвертирова Синие бумажки в Желтые бумажки по установленному курсу\n" +
                $"Введите 5 если хотите конвертирова Желтые бумажки в Красные бумажки по установленному курсу\n" +
                $"Введите 6 если хотите конвертирова Желтые бумажки в Синие бумажки по установленному курсу\n" +
                $"Введите 7 если хотите закончить работу");

                userCommand = Convert.ToInt32(Console.ReadLine());
                switch (userCommand)
                {
                    case 1:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > redPaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            redPaperCount -= moneyForChange;
                            bluePaperCount += moneyForChange * blueToRed;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > redPaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            redPaperCount -= moneyForChange;
                            yellowPaperCount += moneyForChange * yellowToRed;
                        }
                        break;
                    case 3:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > bluePaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            bluePaperCount -= moneyForChange;
                            redPaperCount += moneyForChange / blueToRed;
                        }
                        break;
                    case 4:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > bluePaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            bluePaperCount -= moneyForChange;
                            yellowPaperCount += moneyForChange * yellowToBlue;
                        }
                        break;
                    case 5:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > yellowPaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            yellowPaperCount -= moneyForChange;
                            redPaperCount += moneyForChange / yellowToRed;
                        }
                        break;
                    case 6:
                        Console.WriteLine("Какую сумму Вы хотите обменять?");
                        moneyForChange = Convert.ToSingle(Console.ReadLine());
                        if (moneyForChange > yellowPaperCount)
                        {
                            Console.WriteLine("Недостаточно денег на балансе. Нажмите любую кнопку для возвращения в стартовое меню.");
                            Console.ReadKey();
                        }
                        else
                        {
                            yellowPaperCount -= moneyForChange;
                            bluePaperCount += moneyForChange / yellowToBlue;
                        }
                        break;
                    case 7:
                        break;
                    default:
                        {
                            Console.WriteLine("Неверная команда. Нажмите любую кнопку для возвращения в стартовое меню");
                            Console.ReadKey();
                            break;
                        }
                }
                Console.Clear();
            }
        }
    }
}
