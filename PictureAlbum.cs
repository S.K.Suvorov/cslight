﻿using System;

namespace CSLight
{
    class PictureAlbum
    {
        static void Main()
        {
            int pictureCount = 52;
            int pictureLineCount;
            int pictureInNotFullLine;
            pictureLineCount = pictureCount / 3;
            pictureInNotFullLine = pictureCount % 3;
            Console.WriteLine($"Всего картинок в альбоме - {pictureCount}шт,полностью заполненных рядов в альбоме - {pictureLineCount}шт," +
                $"количество картинок в не до конца заполненном ряду {pictureInNotFullLine}шт");
            Console.ReadKey();
        }
    }
}
