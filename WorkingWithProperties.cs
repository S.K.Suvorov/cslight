﻿using System;

namespace WorkingWithProperties
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player(3,3);
            Renderer renderer = new Renderer();
            Random random = new Random();

            for (int i=0;i<5;i++)
            {
                renderer.DrowPlayer(player.X,player.Y);
                player.SetX(random.Next(0,25));
                player.SetY(random.Next(0,25));
                Console.ReadKey();
                Console.Clear();
            }            
        }
    }
   
    class Player
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Player(int x, int y)
        {
            X = x;
            X = y;
        }
        
        public void SetX(int x)
        {
            X = x;
        }

        public void SetY(int y)
        {
            Y = y;
        }
    }

    class Renderer
    {
        public void DrowPlayer(int x, int y)
        {
            Console.SetCursorPosition(x,y);
            Console.Write('H');
        }
    }
}