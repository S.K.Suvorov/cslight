﻿using System;

namespace LocalMax
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[30];
            Random random = new Random();

            Console.WriteLine("Массив целых чисел:\n");
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(0, 100);
                Console.Write(array[i] + " ");
            }
            Console.WriteLine("\n");

            if (array[0] > array[1])
                Console.WriteLine($"Локальным максимумом является  {array[0]},элемент находящийся на  0 месте в массиве"); // проверка первого элемента

            for (int i = 1; i < (array.Length - 1); i++) // проверка со 2-го по предпоследний элемент
            {
                if ((array[i] > array[i - 1]) && (array[i] > array[i + 1]))
                    Console.WriteLine($"Локальным максимумом является  {array[i]}, элемент находящийся на  {i} месте в массиве");
            }

            if (array[array.Length - 1] > array[array.Length - 2])
                Console.WriteLine($"Локальным максимумом является  {array[array.Length - 1]},элемент находящийся на {array.Length - 1} месте в массиве"); // проверка последнего элемен

            Console.ReadKey();
        }
    }
}
