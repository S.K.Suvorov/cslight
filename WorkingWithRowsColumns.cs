﻿using System;

namespace WorkingWithRowsColumns
{
    class Program
    {
        static void Main(string[] args)
        {
            int row = 2;
            int colum = 1;
            int startRow = 0;
            int startColum = 0;
            int sum = 0;
            int multiplication = 1;
            Random random = new Random();

            Console.WriteLine("Задайте размерность массива.\nВведите количество строк:");
            while ((startRow - 1) < row)
            {
                startRow = Convert.ToInt32(Console.ReadLine());
                if ((startRow - 1) < row)
                    Console.WriteLine($"Слишком маленькое значение. Минимальное количество строк составляет {row + 1}.\nВведите количество строк:");
            }
            Console.WriteLine();
            Console.WriteLine("Введите количество столбцов:");
            while ((startColum - 1) < colum)
            {
                startColum = Convert.ToInt32(Console.ReadLine());
                if ((startColum - 1) < colum)
                    Console.WriteLine($"Слишком маленькое значение. Минимальное количество столбцов составляет {colum + 1}.\nВведите количество столбцов:");
            }
            Console.WriteLine();
            int[,] array = new int[startRow, startColum];

            for (int i = 0; i < startRow; i++)
            {
                for (int j = 0; j < startColum; j++)
                {
                    array[i, j] = random.Next(0, 10);
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < startRow; i++)
                for (int j = 0; j < startColum; j++)
                {
                    if (i == row)
                        sum += array[i, j];
                    if (j == colum)
                        multiplication *= array[i, j];
                }

            Console.WriteLine($"\nСумма элементов в строке № {row} = {sum}");
            Console.WriteLine($"Произведение элементов в столбце № {colum} = {multiplication}");
            Console.ReadKey();
        }
    }
}
