﻿using System;

namespace Zoo
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfEnclosure = -1;
            Zoo Zoo = new Zoo(new Enclosure[] { new Enclosure( new Animal[] { new Animal("Рысь", true, "Мяу ШШш"), new Animal("Рысь", false, "Мяу ШШш"), new Animal("Рысь", true, "Мяу ШШш"), new Animal("Рысь", true, "Мяу ШШш") }),
            new Enclosure( new Animal[] { new Animal("Волк", true, "Уууу Ррр"), new Animal("Волк", false, "Уууу Ррр"), new Animal("Волк", false, "Уууу Ррр"), new Animal("Волк", false, "Уууу Ррр") }),
            new Enclosure( new Animal[] { new Animal("Лошадь", true, "Игого"), new Animal("Лошадь", false, "Игого") }),
            new Enclosure( new Animal [] { new Animal("Лев",true,"Арр")})});

            Console.WriteLine($"Вы видите {Zoo.EnclosuresCount} вальеров. Введите номер вальера к которому хотите подойти или введите выход, для закрытия программы:");

            while (true)
            {
                numOfEnclosure = NumberInput() - 1;

                while (numOfEnclosure < 0 || numOfEnclosure >= Zoo.EnclosuresCount)
                {
                    Console.WriteLine($"Некорректный ввод данных. Введите число от 1 до {Zoo.EnclosuresCount} :");
                    numOfEnclosure = NumberInput() - 1;
                }

                Console.Clear();
                Zoo.ShowEnclosure(numOfEnclosure);
                Console.WriteLine($"\n\nВы видите {Zoo.EnclosuresCount} вальеров. Введите номер вальера к которому хотите подойти или введите выход, для закрытия программы:");
            }
        }

        static int NumberInput()
        {
            int num = 0;
            bool isNum = false;
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine("Некорректный ввод данных. Введите число");
                }
            }
            return num;
        }
    }

    class Zoo
    {
        private Enclosure[] _enclosures;
        public int EnclosuresCount { get; private set; }

        public Zoo(Enclosure[] enclosures)
        {
            _enclosures = enclosures;
            EnclosuresCount = _enclosures.Length;
        }

        public void ShowEnclosure(int EnclosuresNum)
        {
            _enclosures[EnclosuresNum].ShowEnclosure();
        }
    }

    class Animal
    {
        public string AnimalType { get; private set; }
        public string AnimalVoice { get; private set; }
        public bool IsMaleGender { get; private set; }

        public Animal(string animalType, bool isMaleGender, string animalVoice)
        {
            AnimalType = animalType;
            IsMaleGender = isMaleGender;
            AnimalVoice = animalVoice;
        }
    }

    class Enclosure
    {
        public Animal[] Animals { get; private set; }
        public int AnimalCount { get; private set; }
        public int AnimalMaleCount { get; private set; }
        public int AnimalFemaleCount { get; private set; }

        public Enclosure(Animal[] animals)
        {
            if (AreAnimalsIdentical(animals))
            {
                Animals = animals;
                AnimalCount = Animals.Length;
                AnimalMaleCount = GetAnimalMaleCount(animals);
                AnimalFemaleCount = GetAnimalFemaleCount(animals);
            }
        }

        public void ShowEnclosure()
        {
            if (Animals != null)
            {
                Console.WriteLine($"На табличке у вальера написано { Animals[0].AnimalType}\n" +
                    $"В вальере живут {AnimalCount} животных:\n" +
                    $"{AnimalMaleCount} самцов\n" +
                    $"{AnimalFemaleCount} самок\n" +
                    $"Из вальера доносятся звуки: {Animals[0].AnimalVoice}");
            }
            else
            { Console.WriteLine("Вальер пуст"); }
        }

        private bool AreAnimalsIdentical(Animal[] animals)
        {
            bool areAnimalsIdentical = true;
            string animalType = animals[0].AnimalType;
            for (int i = 0; i < animals.Length; i++)
            {
                if (animals[i].AnimalType != animalType)
                    areAnimalsIdentical = false;
            }
            return areAnimalsIdentical;
        }

        private int GetAnimalMaleCount(Animal[] animals)
        {
            int animalMaleCount = 0;
            for (int i = 0; i < animals.Length; i++)
            {
                if (animals[i].IsMaleGender)
                    animalMaleCount++;
            }
            return animalMaleCount;
        }

        private int GetAnimalFemaleCount(Animal[] animals)
        {
            int animalFemaleCount = 0;
            for (int i = 0; i < animals.Length; i++)
            {
                if (!animals[i].IsMaleGender)
                    animalFemaleCount++;
            }
            return animalFemaleCount;
        }
    }
}
