﻿using System;

namespace UIElement
{
    class Program
    {
        static void Main(string[] args)
        {
            int xPos;
            int yPos;
            int barFulling;

            Console.WriteLine("Введите координату Х:");
            xPos = NumderInput(0, 10);
            Console.WriteLine("Введите координату Y:");
            yPos = NumderInput(0, 10);
            Console.WriteLine("Введите процент заполненности шкалы GreenBar:");
            barFulling = NumderInput(0, 100);
            Console.Clear();

            DrawBar(xPos, yPos, barFulling);
            Console.ReadKey();
        }

        static void DrawBar(int xPos, int yPos, int barFulling)
        {
            string bar = "";
            char fullSymbol = '#';
            char emptySymbol = '_';
            int numberOfSymbols=0 ;
            ConsoleColor defaultColor = Console.BackgroundColor;
            ConsoleColor color = ConsoleColor.Green;

            Console.SetCursorPosition(xPos, yPos);
            numberOfSymbols += barFulling / 10;
            if ((barFulling % 10) > 5)
                numberOfSymbols += 1;
            for (int i = 0; i < numberOfSymbols; i++)
                bar += fullSymbol;
            Console.Write("[");
            Console.BackgroundColor = color;
            Console.Write(bar);
            Console.BackgroundColor = defaultColor;
            bar = "";
            for (int i = 0; i < (10 - numberOfSymbols); i++)
                bar += emptySymbol;
            Console.Write(bar + "]\n");
        }

        static int NumderInput(int numMin, int numMax)
        {
            int num= numMin - 1;
            bool isNum = false;

            while ((isNum != true) || (num<=numMin) || (num>=numMax))
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if ((isNum != true) || (num <= numMin) || (num >= numMax))
                {
                    Console.WriteLine($"Некорректный ввод данных. Введите число от {numMin} до {numMax}");
                }
            }
            return num;
        }
    }
}
