﻿using System;

namespace KansasCityShuffle
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numArray = new int[10];
            Random random = new Random();

            Console.WriteLine("Массив целых чисел:\n");
            for (int i = 0; i < numArray.Length; i++)
            {
                numArray[i] = random.Next(1, 100);
                Console.Write(numArray[i] + " ");
            }
            Console.WriteLine("\n");

            Shuffle(numArray);
            Console.WriteLine("Перемешиваем:\n");
            for (int i = 0; i < numArray.Length; i++)
                Console.Write(numArray[i] + " ");
        }

        static void Shuffle(int[] numArray)
        {
            Random random = new Random();
            int tempValue;
            int randomIndex = random.Next(0, numArray.Length);

            for (int i = 0; i < numArray.Length; i++)
            {
                randomIndex = random.Next(0, numArray.Length);
                tempValue = numArray[i];
                numArray[i] = numArray[randomIndex];
                numArray[randomIndex] = tempValue;
            }
        }
    }
}
