﻿using System;

namespace BossFight
{
    class Program
    {
        static void Main(string[] args)
        {
            string spell;
            Random random = new Random();
            int dragonHealth = 1500;
            int maxPlayerHealth = 500;
            int playerHealth = 500;
            int dragonFire;
            int dragonHit;
            int fireBoll;
            int spellHeal;
            int sacredFlame;
            int flameCoolDawn=-1;
            string flameInfo = "Готово к использованию";
            int smokeTime=-1;
            string smokeInfo = "Дым не скрывает персонажа в данный момент";
            string history = "Перед тобой дракон, охраняющий свои сокровища, готовый напасть на тебя. Защищайся или умрешь!!!\n";
            int step = 1;
            bool dragonStep = false;

            Console.WriteLine( history);
            history += $"Ход {step}\n";
            while (dragonHealth > 0 && playerHealth > 0)
            {
                Console.WriteLine($"Ход {step}\n\n" +
                    $"Здоровье Дракона   {dragonHealth}\n" +
                    $"Здоровье Персонажа {playerHealth}\n");
                if (smokeTime == 0)
                {
                    Console.WriteLine("Дым рассеялся, ты встретился взглядом с драконом. Передышка окончилась\n");
                    history += "Дым рассеялся, ты встретился взглядом с драконом. Передышка окончилась\n";
                    smokeInfo = "Дым не скрывает персонажа в данный момент";
                        smokeTime--;
                }
                Console.WriteLine($"Ты можешь использовать следующие заклинания:" +
                    $"\n FireBoll -  чтобы послать в дракона огненный шар" +
                    $"\n Smoke - вызать дым, который заволокет локацию, и скроет противников друг от друга на некоторое время. {smokeInfo} " +
                    $"\n Heal - заклинание здоровья, на его сотворение требуется время, так что лучше спрятаться от противника чтобы сотворить его" + 
                    $"\n SacredFlame - залей все священым пламенем, после него требуется некоторый отдых, чтобы повторно применить его. {flameInfo}");

                spell = Console.ReadLine();
                Console.Clear();
                switch(spell)
                {
                    case "FireBoll":
                        if (smokeTime <= 0)
                        {
                            fireBoll = random.Next(75, 175);
                            dragonHealth -= fireBoll;
                            Console.WriteLine($"Ты сотворил огненный шар и нанес {fireBoll} урона");
                            history += $"Ты сотворил огненный шар и нанес {fireBoll} урона\n";
                        }
                        else
                            Console.WriteLine("Поле заволокло дымом, дракона не видно");
                        dragonStep = true;
                        break;
                    case "Smoke":
                        smokeTime = 3;
                        dragonStep = true;
                        break;
                    case "Heal":
                        if (smokeTime > 0)
                        {
                            spellHeal = random.Next(75, 250);
                            if (playerHealth + spellHeal > maxPlayerHealth)
                                spellHeal = maxPlayerHealth - playerHealth;
                            playerHealth += spellHeal;
                            Console.WriteLine($"Используя заклинание Исцеления ты излечил {spellHeal} ран");
                            history += $"Используя заклинание Исцеления ты излечил {spellHeal} ран\n";
                            dragonStep = true;
                        }
                        else
                        {
                            Console.WriteLine("Слишком рисковано нужен дым.Нажми любую кнопку");
                            Console.ReadKey();
                            Console.Clear();
                            dragonStep = false; 
                            
                        }
                        break;
                    case "SacredFlame":
                        if (smokeTime <= 0 && flameCoolDawn<=0)
                        {
                            sacredFlame = random.Next(250, 400);
                            dragonHealth -= sacredFlame;
                            Console.WriteLine($"Ты залил все священным пламенем и нанес {sacredFlame} урона");
                            history += $"Ты залил все священным пламенем и нанес {sacredFlame} урона\n";
                            dragonStep = true;
                            flameCoolDawn = 4;

                        }
                        else if (smokeTime > 0)
                        {
                            Console.WriteLine("Поле заволокло дымом, дракона не видно");
                            dragonStep = true;
                        }
                        else if (flameCoolDawn>0 )
                        {
                            dragonStep = false;
                            Console.WriteLine("Не готово к использованию.Нажми любую кнопку для повтора.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    default:
                        Console.WriteLine("Герой пробубнил нечто невнятное. Ничего не произошло.");
                        dragonStep = true;
                        break;
                }

                if (dragonStep==true && smokeTime<=0)
                {
                    if (random.Next(0,5)<3)
                    {
                        dragonHit = random.Next(75, 100);
                        playerHealth -= dragonHit;
                        Console.WriteLine($"Дракон нанес удар когтями на {dragonHit}\n");
                        history += $"Дракон нанес удар когтями на {dragonHit}\n";
                    }
                    else
                    {
                        dragonFire = random.Next(50, 250);
                        playerHealth -= dragonFire;
                        Console.WriteLine($"Дракон нанес урон огненным дыханием на {dragonFire}\n");
                        history += $"Дракон нанес урон огненным дыханием на {dragonFire}\n";
                    }
                }
                if (dragonStep == true )
                {
                    dragonStep = false;
                    step++;
                    history += $"Ход {step}\n";
                    if (smokeTime > 0)
                    {
                        smokeInfo = $"Поле затянуто дымом, ничего не будет видно следующие {smokeTime} ходов";
                        smokeTime--;
                    }
                    if (flameCoolDawn > 0)
                    {
                        flameInfo = $" Будет доступно через {flameCoolDawn} ходов";
                        flameCoolDawn--;
                    }
                    else flameInfo = "Готово к использованию";
                }

                if (playerHealth <= 0 && dragonHealth <= 0)
                {
                    Console.WriteLine("Герой и дракон извели друг друга");
                    history += "\nГерой и дракон извели друг друга";
                }
                if (playerHealth <= 0)
                {
                    Console.WriteLine("Ты умер. Дракон победил");
                    history += "\nТы умер. Дракон победил";

                }
                if (dragonHealth <= 0)
                {
                    Console.WriteLine("Ты победил. Золото дракона теперь твоё");
                    history += "\nТы победил. Золото дракона теперь твоё";
                }
            }

            Console.Clear();
            Console.WriteLine(history);
            Console.ReadKey();
        }
    }
}
