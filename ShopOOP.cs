﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            string command = " ";
            Player player = new Player(new Goods[] {
            new Goods("Меч",1,1000),
            new Goods("Щит",1, 500)}, 1000);
            Shop shop = new Shop(new Goods[]{
                new Goods("Зелье Силы", 5, 50),
                new Goods("Зелье Ловкости", 5, 75),
                new Goods("Зелье Здоровья", 5, 25),
                new Goods("Зелье Удачи", 5, 100) });

            while (command != "выход")
            {
                shop.ShowGoodsList();

                Console.WriteLine("\n\nВведите команду для действия: " +
                    "\nКупить - купить товар из списка " +
                    "\nИнвентарь - открыть инвентарь персонажа " +
                    "\nВыход - выйти из магазина");
                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "купить":
                        shop.SellGoods(player);
                        Console.Clear();
                        break;
                    case "инвентарь":
                        Console.Clear();
                        player.ShowPlayerInventory();
                        Console.WriteLine("Нажми любую кнопку для возврата в меню");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case "выход":
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Некоректная команда, попробуй еще раз.Нажми любую кнопку для возврата в меню");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }
    }


    class Goods
    {
        public int Price { get; }
        public string ProductName { get; }
        public int ProductNameCount { get; private set; }

        public Goods(string productName, int productNameCount, int price)
        {
            Price = price;
            ProductName = productName;
            ProductNameCount = productNameCount;
        }

        public void ShowGoods()
        {
            Console.WriteLine($"{ProductName} в наличие {ProductNameCount} цена {Price}");
        }

        public void BuyGoods(int count)
        {
            ProductNameCount += count;
        }

        public void SellGoods(int count)
        {
            ProductNameCount -= count;
        }

    }

    class Shop
    {
        private Goods[] _shopGoods;

        public Shop(Goods[] ShopGoods)
        {
            _shopGoods = ShopGoods;
        }

        private bool SeachGoods(ref int num)
        {
            bool seachresult = false;
            string goodsForSeach;

            Console.WriteLine("Введите название товара:");
            goodsForSeach = Console.ReadLine().ToLower();

            for (int i = 0; i < _shopGoods.Length; i++)
            {
                if (goodsForSeach == _shopGoods[i].ProductName.ToLower())
                {
                    seachresult = true;
                    num = i;
                    break;
                }
            }

            if (!seachresult)
            {
                Console.Clear();
                Console.WriteLine("Такого товара не найдено в магазине. Нажмите любую кнопку для возврата в меню");
                Console.ReadKey();
                Console.Clear();
            }
            return seachresult;
        }

        private int NumderInput(int MaxValue)
        {
            int num = 0;
            bool isNum = false;
            Console.WriteLine($"Введите количество товара, которое хотите купить. Сейчас доступно  {MaxValue} шт. товата");
            while (isNum != true)
            {
                isNum = Int32.TryParse(Console.ReadLine(), out num);
                if (isNum != true)
                {
                    Console.WriteLine($"Некорректный ввод данных. Введите число от 0 до {MaxValue}");
                }
                else if (num < 0 || num > MaxValue)
                {
                    Console.WriteLine($"Столько товара в наличии нет. Введите число от 0 до {MaxValue}");
                    isNum = false;
                }
            }
            return num;
        }

        public void SellGoods(Player PlayerInventory)
        {
            int sellGoodsCount;
            int goodsNumber = 0;

            if (SeachGoods(ref goodsNumber))
            {
                sellGoodsCount = NumderInput(_shopGoods[goodsNumber].ProductNameCount);
                if (PlayerInventory.Money >= (sellGoodsCount * _shopGoods[goodsNumber].Price))
                {
                    PlayerInventory.BuyGoods(new Goods(_shopGoods[goodsNumber].ProductName, sellGoodsCount, _shopGoods[goodsNumber].Price));
                    _shopGoods[goodsNumber].SellGoods(sellGoodsCount);
                }
                else
                {
                    Console.WriteLine("Недостаточно денег для покупки. Нажмите любую кнопку для возврата в меню");
                    Console.ReadKey();
                    Console.Clear();
                }

                if (_shopGoods[goodsNumber].ProductNameCount == 0)
                {
                    Goods[] tempGoods = new Goods[_shopGoods.Length - 1];
                    int shift = 0;
                    for (int i = 0; i < tempGoods.Length; i++)
                    {
                        if (i == goodsNumber)
                            shift = 1;
                        tempGoods[i] = _shopGoods[i + shift];
                    }
                    _shopGoods = tempGoods;
                }
            }
        }

        public void ShowGoodsList()
        {
            Console.WriteLine("\nСписок товаров в лавке:");
            for (int i = 0; i < _shopGoods.Length; i++)
            {
                _shopGoods[i].ShowGoods();
            }
        }
    }

    class Player
    {
        private Goods[] _playerGoods;
        public int Money { get; private set; }

        public Player(Goods[] PlayerGoods, int money)
        {
            _playerGoods = PlayerGoods;
            Money = money;
        }

        public void ShowPlayerInventory()
        {
            Console.WriteLine("\nСписок предметов в инвентаре:");
            for (int i = 0; i < _playerGoods.Length; i++)
            {
                _playerGoods[i].ShowGoods();
            }
            Console.WriteLine("\nКоличество золота у игрока:" + Money);
        }

        public void BuyGoods(Goods shopGoods)
        {
            int goodsNum = 0;

            if (SeachGoods(ref goodsNum, shopGoods.ProductName))
            {
                _playerGoods[goodsNum].BuyGoods(shopGoods.ProductNameCount);
            }
            else
            {
                Goods[] tempGoods = new Goods[_playerGoods.Length + 1];
                for (int i = 0; i < _playerGoods.Length; i++)
                    tempGoods[i] = _playerGoods[i];
                tempGoods[tempGoods.Length - 1] = shopGoods;
                _playerGoods = tempGoods;
            }

            Money -= shopGoods.ProductNameCount * shopGoods.Price;
        }

        private bool SeachGoods(ref int num, string goodsForSeach)
        {
            bool seachresult = false;

            for (int i = 0; i < _playerGoods.Length; i++)
            {
                if (goodsForSeach == _playerGoods[i].ProductName)
                {
                    seachresult = true;
                    num = i;
                    break;
                }
            }
            return seachresult;
        }
    }
}
