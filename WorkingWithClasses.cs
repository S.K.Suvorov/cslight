﻿using System;

namespace WorkingWithClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player("SuperPuper");
            player1.PlayerInfo();
        }
    }
    class Player
    {
        public string NickName;
        public int PlayerLavel;
        public bool IsActive;
        public bool InBanned;

        public Player(string nickName)
        {
            NickName = nickName;
            PlayerLavel = 0;
            IsActive = true;
            InBanned = false;
        }
        public void PlayerInfo()
        {
            Console.WriteLine("Ник - " + NickName + "\nУровень игрока - " + PlayerLavel);
            if (IsActive)
                Console.WriteLine("В сети");
            else
                Console.WriteLine("Не в сети");
            if (InBanned)
                Console.WriteLine("Забанен");
            else
                Console.WriteLine("Имеет доступ");

        }
    }
}
