﻿using System;

namespace DynamicArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            string command="";
            bool isNum;
            int[] array = new int[0];
            int sum = 0;

            while (command != "exit")
            {
                Console.SetCursorPosition(1, 10);
                if(array.Length>0)
                {
                    Console.WriteLine("Элементы массива:\n");
                    for (int i = 0; i < array.Length; i++)
                        Console.Write(array[i]+" ");
                }
                Console.SetCursorPosition(1, 1);
                Console.WriteLine("Для подачи команд введите:\n" +
                    "Любое число для занесение его в масив\n" +
                    "sum - для суммы элементов массива\n" +
                    "exit - для выхода из программы \n");
                command = Console.ReadLine();

                switch(command)
                {
                    case "sum":
                        sum = 0;
                        for (int i = 0; i < array.Length; i++) 
                        sum += array[i];
                        Console.WriteLine("Сумма всех элементов равна " + sum);
                        Console.WriteLine("Нажмите Enter для продолжения");
                        Console.ReadLine();
                        break;
                    case "exit":
                        break;
                    default:
                        isNum = Int32.TryParse(command, out num);
                        if (isNum == true)
                        {
                            int[] tempArray = new int[(array.Length + 1)];
                            for (int i = 0; i < array.Length; i++)
                                tempArray[i] = array[i];
                            tempArray[tempArray.Length-1] = num;
                            array = tempArray;
                        }
                        else
                        {
                            Console.WriteLine("Некорректная команда. Нажмите Enter для продолжения");
                            Console.ReadLine();
                        }
                        break;
                }
                Console.Clear();
            }
        }
    }
}
